'use strict';
module.exports = function (app) {
    var uploadController = require('../controllers/Upload');
    app.route('/import/companies')
        .post(uploadController.importCompanies);
    app.route('/upload/companies')
        .post(uploadController.uploadCompanies);
    app.route('/downloadSampleCompany')
        .get(uploadController.downloadSampleCompany);
    app.route('/downloadSamplePersonnel')
        .get(uploadController.downloadSamplePersonnel);
    app.route('/upload/techno')
        .post(uploadController.uploadTechno);
    app.route('/upload/googleIndoCompanies')
        .post(uploadController.uploadGoogleIndonesiaCompanies);
};
