'use strict';

const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const axios = require('axios');
const XLSX = require('xlsx');

//models
const db = require('../models/db');
const Company = require('../models/Company');
const CompanyItem = require('../models/CompanyItem');
const Personnel = require('../models/Personnel');

//util
const paginate = require('../utils/pagination');

//config
const config = require('../../config');

exports.totalDigitalEngagement = async function (req, res) {

    let file_name = req.body.file_name;
    let dataset = req.body.dataset;
    let frimographicFilter = req.body.frimographicFilter;

    let whereFilter = [{ dataset }];

    if (file_name !== "Master DB (Golden Source)") whereFilter.push({ file_name });

    frimographicFilter && frimographicFilter.industry && whereFilter.push({ industry: { [Op.or]: frimographicFilter.industry } })
    frimographicFilter && frimographicFilter.main_hq_location && whereFilter.push({ main_hq_location: { [Op.or]: frimographicFilter.main_hq_location } })
    frimographicFilter && frimographicFilter.emp_size && whereFilter.push({ total_personnel: { [Op.or]: frimographicFilter.emp_size } })

    let results = [];

    try {
        const companies = await Company.findAll({
            where: {
                [Op.and]: whereFilter
            },
            attributes: ["overall_knapshot_score"]
        });

        let basic = 0;
        let intermediate = 0;
        let high = 0;
        let advance = 0;

        if (companies) {
            for (let i = 0; i < companies.length; i++) {
                let score = companies[i].overall_knapshot_score;

                if (score < 2) basic += 1;
                if (score >= 2 && score < 5) intermediate += 1;
                if (score >= 5 && score < 8) high += 1;
                if (score >= 8) advance += 1;
            }
        }

        results.push(
            { label: "Basic", count: basic },
            { label: "Intermediate", count: intermediate },
            { label: "High", count: high },
            { label: "Advanced", count: advance },
        );

        return res.status(200).json({
            message: "Successful",
            data: results,
            count: companies.length
        });
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

exports.industryBreakDown = async function (req, res) {

    const datasetName = req.body.dataset;
    const industryNames = req.body.industries;
    const digitalNames = req.body.digitals;
    const fileName = req.body.file_name;

    let totalIndustries = [];
    let results = [];

    try {
        for (let i = 0; i < industryNames.length; i++) {

            let obj = {
                label: industryNames[i],
                count: 0,
                basic: 0,
                intermediate: 0,
                high: 0,
                advanced: 0
            };

            let industryQuery = [
                { industry: industryNames[i] },
                { dataset: datasetName }
            ];
            let basicQuery = [
                { overall_knapshot_score: { [Op.lt]: 2 } },
                { dataset: datasetName },
                { industry: industryNames[i] }
            ];
            let intermediateQuery = [
                { overall_knapshot_score: { [Op.gte]: 2 } },
                { overall_knapshot_score: { [Op.lt]: 5 } },
                { industry: industryNames[i] },
                { dataset: datasetName }
            ];
            let highQuery = [
                { overall_knapshot_score: { [Op.gte]: 5 } },
                { overall_knapshot_score: { [Op.lt]: 8 } },
                { industry: industryNames[i] },
                { dataset: datasetName }
            ];
            let advancedQuery = [
                { overall_knapshot_score: { [Op.gte]: 8 } },
                { industry: industryNames[i] },
                { dataset: datasetName },
            ];

            if (fileName !== "Master DB (Golden Source)") {
                industryQuery.push({ file_name: fileName });
                basicQuery.push({ file_name: fileName });
                intermediateQuery.push({ file_name: fileName });
                highQuery.push({ file_name: fileName });
                advancedQuery.push({ file_name: fileName });
            }

            let industries = await CompanyItem.findAll({ where: { [Op.and]: industryQuery } });

            if (industries) {

                totalIndustries.push({ name: industryNames[i], count: industries.length });
                obj.count = industries.length;

                if (digitalNames.indexOf("Basic") >= 0) {
                    let basic = await CompanyItem.count({ where: { [Op.and]: basicQuery } });
                    obj.basic = basic;
                }

                if (digitalNames.indexOf("Intermediate") >= 0) {
                    let intermediate = await CompanyItem.count({ where: { [Op.and]: intermediateQuery } });
                    obj.intermediate = intermediate;
                }

                if (digitalNames.indexOf("High") >= 0) {
                    let high = await CompanyItem.count({ where: { [Op.and]: highQuery } });
                    obj.high = high;
                }

                if (digitalNames.indexOf("Advanced") >= 0) {
                    let advance = await CompanyItem.count({ where: { [Op.and]: advancedQuery } });
                    obj.advanced = advance;
                }

                results.push(obj);
            }
        }

        return res.status(200).json({
            message: "Successful",
            results: results,
            totalIndustries: totalIndustries
        });
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

exports.getDigitalPresentByCountry = async function (req, res) {

    const dataset = req.body.datasets;
    const filename = req.body.fileName;

    let results = {};

    let types = [
        {
            type: "Basic",
            query: {
                overall_knapshot_score: {
                    [Op.lt]: 2
                }
            }
        },
        {
            type: "Intermediate",
            query: {
                overall_knapshot_score: {
                    [Op.gte]: 2
                },
                overall_knapshot_score: {
                    [Op.lt]: 5
                },
            }
        },
        {
            type: "High",
            query: {
                overall_knapshot_score: {
                    [Op.gte]: 5
                },
                overall_knapshot_score: {
                    [Op.lt]: 8
                },
            }
        },
        {
            type: "Advanced",
            query: {
                overall_knapshot_score: {
                    [Op.gte]: 8
                },
            }
        }
    ];

    if (filename !== "Master DB (Golden Source)") {
        types.map(t => {
            t["query"] = { ...t["query"], file_name: filename };
        });
    }

    try {
        for (let j = 0; j < types.length; j++) {

            const count = await CompanyItem.count({
                where: {
                    [Op.and]: [
                        {
                            dataset: dataset
                        },
                        types[j].query
                    ]
                }
            });

            const noUrl_noDirPsc_noSocial = await CompanyItem.count({
                where: {
                    [Op.and]: [
                        {
                            dataset: dataset
                        },
                        {
                            website: {
                                [Op.eq]: null
                            }
                        },
                        {
                            no_of_directory_presence: {
                                [Op.lte]: 0
                            }
                        },
                        {
                            [Op.and]: [
                                {
                                    facebook: {
                                        [Op.eq]: null
                                    }
                                },
                                {
                                    linkedIn: {
                                        [Op.eq]: null
                                    }
                                },
                                {
                                    twitter: {
                                        [Op.eq]: null
                                    }
                                },
                                {
                                    instagram: {
                                        [Op.eq]: null
                                    }
                                }
                            ]
                        },
                        types[j].query
                    ]
                }
            });

            const noUrl_hasDirPsc_noSocial = await CompanyItem.count({
                where: {
                    [Op.and]: [
                        {
                            dataset: dataset
                        },
                        {
                            website: {
                                [Op.eq]: null
                            }
                        },
                        {
                            no_of_directory_presence: {
                                [Op.gt]: 0
                            }
                        },
                        {
                            [Op.and]: [
                                {
                                    facebook: {
                                        [Op.eq]: null
                                    }
                                },
                                {
                                    linkedIn: {
                                        [Op.eq]: null
                                    }
                                },
                                {
                                    twitter: {
                                        [Op.eq]: null
                                    }
                                },
                                {
                                    instagram: {
                                        [Op.eq]: null
                                    }
                                }
                            ]
                        },
                        types[j].query
                    ]
                }
            });

            const noUrl_hasDirPsc_hasSocial = await CompanyItem.count({
                where: {
                    [Op.and]: [
                        {
                            dataset: dataset
                        },
                        {
                            website: {
                                [Op.eq]: null
                            }
                        },
                        {
                            no_of_directory_presence: {
                                [Op.gt]: 0
                            }
                        },
                        {
                            [Op.or]: [
                                {
                                    facebook: {
                                        [Op.ne]: null
                                    }
                                },
                                {
                                    linkedIn: {
                                        [Op.ne]: null
                                    }
                                },
                                {
                                    twitter: {
                                        [Op.ne]: null
                                    }
                                },
                                {
                                    instagram: {
                                        [Op.ne]: null
                                    }
                                }
                            ]
                        },
                        types[j].query
                    ]
                }
            });

            const hasUrl_noDirPsc_noSocial = await CompanyItem.count({
                where: {
                    [Op.and]: [
                        {
                            dataset: dataset
                        },
                        {
                            website: {
                                [Op.ne]: null
                            }
                        },
                        {
                            no_of_directory_presence: {
                                [Op.lte]: 0
                            }
                        },
                        {
                            [Op.or]: [
                                {
                                    facebook: {
                                        [Op.eq]: null
                                    }
                                },
                                {
                                    linkedIn: {
                                        [Op.eq]: null
                                    }
                                },
                                {
                                    twitter: {
                                        [Op.eq]: null
                                    }
                                },
                                {
                                    instagram: {
                                        [Op.eq]: null
                                    }
                                }
                            ]
                        },
                        types[j].query
                    ]
                }
            });

            const hasUrl_hasDirPsc_noSocial = await CompanyItem.count({
                where: {
                    [Op.and]: [
                        {
                            dataset: dataset
                        },
                        {
                            website: {
                                [Op.ne]: null
                            }
                        },
                        {
                            no_of_directory_presence: {
                                [Op.gt]: 0
                            }
                        },
                        {
                            [Op.and]: [
                                {
                                    facebook: {
                                        [Op.eq]: null
                                    }
                                },
                                {
                                    linkedIn: {
                                        [Op.eq]: null
                                    }
                                },
                                {
                                    twitter: {
                                        [Op.eq]: null
                                    }
                                },
                                {
                                    instagram: {
                                        [Op.eq]: null
                                    }
                                }
                            ]
                        },
                        types[j].query
                    ]
                }
            });

            const hasUrl_hasDirPsc_hasSocial = await CompanyItem.count({
                where: {
                    [Op.and]: [
                        {
                            dataset: dataset
                        },
                        {
                            website: {
                                [Op.ne]: null
                            }
                        },
                        {
                            no_of_directory_presence: {
                                [Op.gt]: 0
                            }
                        },
                        {
                            [Op.or]: [
                                {
                                    facebook: {
                                        [Op.ne]: null
                                    }
                                },
                                {
                                    linkedIn: {
                                        [Op.ne]: null
                                    }
                                },
                                {
                                    twitter: {
                                        [Op.ne]: null
                                    }
                                },
                                {
                                    instagram: {
                                        [Op.ne]: null
                                    }
                                }
                            ]
                        },
                        types[j].query
                    ]
                }
            });

            results[types[j]["type"]] = [
                types[j]["type"],
                count,
                noUrl_noDirPsc_noSocial,
                noUrl_hasDirPsc_noSocial,
                noUrl_hasDirPsc_hasSocial,
                hasUrl_noDirPsc_noSocial,
                hasUrl_hasDirPsc_noSocial,
                hasUrl_hasDirPsc_hasSocial
            ];
        }
        return res.status(200).json({ message: 'Successful', results: results });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

exports.totalIndustry = async function (req, res) {

    let dataset = req.body.dataset;
    let file_name = req.body.file_name;

    let industryName = [];
    let totalIndustry = 0;
    let industries;

    try {
        if (file_name !== "Master DB (Golden Source)") {
            industries = await db.query(
                `SELECT IF(industry IS NULL OR industry = '', 'Not Available', industry) industry, COUNT(1) as count FROM company WHERE dataset IN(:datasets) and file_name LIKE :file_name GROUP BY 1 ORDER BY count DESC`,
                {
                    replacements: { datasets: dataset, file_name: file_name },
                    type: db.QueryTypes.SELECT
                }
            );
        } else {
            industries = await db.query(
                `SELECT IF(industry IS NULL OR industry = '', 'Not Available', industry) industry, COUNT(1) as count FROM company WHERE dataset IN(:datasets) GROUP BY 1 ORDER BY count DESC`,
                {
                    replacements: { datasets: dataset },
                    type: db.QueryTypes.SELECT
                }
            );
        }

        for (let i = 0; i < industries.length; i++) {
            industryName.push(industries[i].industry);
            totalIndustry += industries[i].count
        }

        return res.status(200).json({
            message: "Successful",
            results: {
                industries: industries,
                totalIndustry: totalIndustry,
                industryName: industryName
            }
        });
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

exports.totalCountry = async function (req, res) {

    let filename = req.body.file_name;

    let countries = [];
    let countryName = [];
    let totalCountry = 0;

    try {

        if (filename !== "Master DB (Golden Source)") {
            countries = await db.query(
                `SELECT dataset, COUNT(1) as count FROM company WHERE file_name LIKE :filename GROUP BY dataset ORDER BY count DESC`,
                {
                    replacements: { filename: filename },
                    type: db.QueryTypes.SELECT
                }
            );
        } else {
            countries = await db.query(
                `SELECT dataset, COUNT(1) as count FROM company GROUP BY dataset ORDER BY count DESC`,
                {
                    type: db.QueryTypes.SELECT
                }
            );
        }

        for (let i = 0; i < countries.length; i++) {
            countryName.push(countries[i].dataset);
            totalCountry += countries[i].count;
        }

        return res.status(200).json({
            message: "Successful",
            results: {
                country: countries,
                totalCountry: totalCountry,
                countryName: countryName
            }
        });

    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

exports.totalPersonnel = async function (req, res) {

    let dataset = req.body.dataset;
    let file_name = req.body.file_name;


    let personnelName = [];
    let totalPersonnel = 0;
    let personnel;

    try {
        if (file_name !== "Master DB (Golden Source)") {
            personnel = await db.query(
                `SELECT IF(total_Personnel IS NULL OR total_Personnel = '' OR total_Personnel = '-1', 'Not Available', total_Personnel) total_Personnel, COUNT(1) as count FROM company WHERE dataset IN(:datasets) and file_name LIKE :file_name GROUP BY 1 ORDER BY count DESC`,
                {
                    replacements: { datasets: dataset, file_name: file_name },
                    type: db.QueryTypes.SELECT
                }
            );
        } else {
            personnel = await db.query(
                `SELECT IF(total_Personnel IS NULL OR total_Personnel = '', 'Not Available', total_Personnel) total_Personnel, COUNT(1) as count FROM company WHERE dataset IN(:datasets) GROUP BY 1 ORDER BY count DESC`,
                {
                    replacements: { datasets: dataset },
                    type: db.QueryTypes.SELECT
                }
            );
        }

        for (let i = 0; i < personnel.length; i++) {

            personnelName.push(personnel[i].total_Personnel);
            totalPersonnel += personnel[i].count
        }

        return res.status(200).json({
            message: "Successful",
            results: {
                personnel: personnel,
                totalPersonnel: totalPersonnel,
                personnelName: personnelName
            }
        });
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

exports.totalCompanyStaff = async function (req, res) {

    let dataset = req.body.dataset;
    let file_name = req.body.file_name;


    let personnelName = [];
    let totalPersonnel = 0;
    let personnel;

    try {
        if (file_name !== "Master DB (Golden Source)") {
            personnel = await db.query(
                `SELECT IF(total_Personnel IS NULL OR total_Personnel = '', 'Company without Staff','Company with Staff') total_Personnel, COUNT(1) as count FROM company WHERE dataset IN(:datasets) and file_name LIKE :file_name GROUP BY 1 ORDER BY count DESC`,
                {
                    replacements: { datasets: dataset, file_name: file_name },
                    type: db.QueryTypes.SELECT
                }
            );
        } else {
            personnel = await db.query(
                `SELECT IF(total_Personnel IS NULL OR total_Personnel = '', 'Company without Staff','Company with Staff') total_Personnel, COUNT(1) as count FROM company WHERE dataset IN(:datasets) GROUP BY 1 ORDER BY count DESC`,
                {
                    replacements: { datasets: dataset },
                    type: db.QueryTypes.SELECT
                }
            );
        }

        for (let i = 0; i < personnel.length; i++) {

            personnelName.push(personnel[i].total_Personnel);
            totalPersonnel += personnel[i].count
        }

        return res.status(200).json({
            message: "Successful",
            results: {
                personnel: personnel,
                totalPersonnel: totalPersonnel,
                personnelName: personnelName
            }
        });
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

exports.totalHQLocation = async function (req, res) {

    let dataset = req.body.dataset;
    let file_name = req.body.file_name;


    let HQLocationName = [];
    let totalHQLocation = 0;
    let HQLocation;

    try {
        if (file_name !== "Master DB (Golden Source)") {
            HQLocation = await db.query(
                `SELECT IF(main_hq_location IS NULL OR main_hq_location = '', 'Not Available', main_hq_location) main_hq_location, COUNT(1) as count FROM company WHERE dataset IN(:datasets) and file_name LIKE :file_name GROUP BY 1 ORDER BY count DESC`,
                {
                    replacements: { datasets: dataset, file_name: file_name },
                    type: db.QueryTypes.SELECT
                }
            );
        } else {
            HQLocation = await db.query(
                `SELECT IF(main_hq_location IS NULL OR main_hq_location = '', 'Not Available', main_hq_location) main_hq_location, COUNT(1) as count FROM company WHERE dataset IN(:datasets) GROUP BY 1 ORDER BY count DESC`,
                {
                    replacements: { datasets: dataset },
                    type: db.QueryTypes.SELECT
                }
            );
        }

        for (let i = 0; i < HQLocation.length; i++) {
            HQLocationName.push(HQLocation[i].main_hq_location);
            totalHQLocation += HQLocation[i].count
        }


        return res.status(200).json({
            message: "Successful",
            results: {
                HQLocation: HQLocation,
                totalHQLocation: totalHQLocation,
                HQLocationName: HQLocationName
            }
        });
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

exports.getEndUserTechnology = async function (req, res) {

    const datasets = req.body.datasets;
    const filename = req.body.fileName;

    let technologies = [
        "All Technology",
        "Advertising",
        "Analytics and Tracking",
        "Ecommerce",
        "Payment",
        "Widgets"
    ];

    let data = {
        "All Technology": [],
        "Advertising": [],
        "Analytics and Tracking": [],
        "Ecommerce": [],
        "Payment": [],
        "Widgets": []
    };

    let header = {
        "Advertising": ["Country", "Total Company", "Ad Network", "ads txt", "Contextual Advertising", "Ad Exchange", "Facebook Exchange", "Retargeting / Remarketing"],
        "Analytics and Tracking": ["Country", "Total Company", "Conversion Optimization", "Tag Management", "Advertiser Tracking", "Audience Measurement", "Lead Generation", "Marketing Automation"],
        "Ecommerce": ["Country", "Total Company", "Non Platform", "Hosted Solution", "Open Source", "Multi-Channel", "Enterprise", "SMB Solution"],
        "Payment": ["Country", "Total Company", "Payments Processor", "Payment Acceptance", "Payment Currency", "Checkout Buttons", "PP, PA, PC, CO", "PA & PC CO"],
        "Widgets": ["Country", "Total Company", "Live Chat", "Customer Login", "Social Sharing", "Schedule Management", "Ticketing System", "Bookings"],
        "All Technology": ["Country", "Total Company", "Advertising", "SEO", "Analytics", "E-commerce", "Payments", "Widgets"]
    }

    try {
        for (let i = 0; i < datasets.length; i++) {

            let companies;

            if (filename !== "Master DB (Golden Source)") {
                companies = await CompanyItem.findAll({
                    attributes: ["asset"],
                    where: {
                        [Op.and]: [
                            { dataset: datasets[i] },
                            {
                                asset: {
                                    [Op.ne]: null
                                }
                            },
                            {
                                filename: fileName
                            }
                        ]
                    }
                });
            } else {
                companies = await CompanyItem.findAll({
                    attributes: ["asset"],
                    where: {
                        [Op.and]: [
                            { dataset: datasets[i] },
                            {
                                asset: {
                                    [Op.ne]: null
                                }
                            }
                        ]
                    }
                });
            }

            let adNetwork = 0;
            let adsTxt = 0;
            let contextualAds = 0;
            let adExchange = 0;
            let fbExchange = 0;
            let retargetingAds = 0;

            let conversionOpt = 0;
            let tagMng = 0;
            let adsTracking = 0;
            let audienceMeasurement = 0;
            let leadGeneration = 0;
            let marketAutomation = 0;

            let nonPlatform = 0;
            let hostedSolution = 0;
            let openSource = 0;
            let multiChannel = 0;
            let enterprise = 0;
            let smbSolution = 0;

            let payProcessor = 0;
            let payAcceptance = 0;
            let payCurrency = 0;
            let checkOurButton = 0;
            let PPPAPCCO = 0;
            let PAPCCO = 0;

            let liveChat = 0;
            let customerLogin = 0;
            let socialSharing = 0;
            let scheduleManagement = 0;
            let ticketingSystem = 0;
            let bookings = 0;

            let totalAdvertisingCount = 0;
            let totalSEOCount = 0;
            let totalAnalyticsCount = 0;
            let totalEcommerceCount = 0;
            let totalPaymentCount = 0;
            let totalWidgetsCount = 0;

            for (let j = 0; j < companies.length; j++) {

                let jsonObj = JSON.parse(companies[j]["asset"]);

                if (jsonObj !== null) {

                    if (jsonObj["Advertising"] !== undefined) {
                        totalAdvertisingCount += 1;
                        if (jsonObj["Advertising"]["Ad Network"] !== undefined) adNetwork += 1;
                        if (jsonObj["Advertising"]["ads txt"] !== undefined) adsTxt += 1;
                        if (jsonObj["Advertising"]["Contextual Advertising"] !== undefined) contextualAds += 1;
                        if (jsonObj["Advertising"]["Ad Exchange"] !== undefined) adExchange += 1;
                        if (jsonObj["Advertising"]["Facebook Exchange"] !== undefined) fbExchange += 1;
                        if (jsonObj["Advertising"]["Retargeting / Remarketing"] !== undefined) retargetingAds += 1;
                    }

                    if (jsonObj["Analytics and Tracking"] !== undefined) {
                        totalAnalyticsCount += 1;
                        if (jsonObj["Analytics and Tracking"]["Conversion Optimization"] !== undefined) conversionOpt += 1;
                        if (jsonObj["Analytics and Tracking"]["Tag Management"] !== undefined) tagMng += 1;
                        if (jsonObj["Analytics and Tracking"]["Advertiser Tracking"] !== undefined) adsTracking += 1;
                        if (jsonObj["Analytics and Tracking"]["Audience Measurement"] !== undefined) audienceMeasurement += 1;
                        if (jsonObj["Analytics and Tracking"]["Lead Generation"] !== undefined) leadGeneration += 1;
                        if (jsonObj["Analytics and Tracking"]["Marketing Automation"] !== undefined) marketAutomation += 1;
                    }

                    if (jsonObj["Ecommerce"] !== undefined) {
                        totalEcommerceCount += 1;
                        if (jsonObj["Ecommerce"]["Non Platform"] !== undefined) nonPlatform += 1;
                        if (jsonObj["Ecommerce"]["Hosted Solution"] !== undefined) hostedSolution += 1;
                        if (jsonObj["Ecommerce"]["Open Source"] !== undefined) openSource += 1;
                        if (jsonObj["Ecommerce"]["Multi-Channel"] !== undefined) multiChannel += 1;
                        if (jsonObj["Ecommerce"]["Enterprise"] !== undefined) enterprise += 1;
                        if (jsonObj["Ecommerce"]["SMB Solution"] !== undefined) smbSolution += 1;
                    }

                    if (jsonObj["Payment"] !== undefined) {
                        totalPaymentCount += 1;
                        if (jsonObj["Payment"]["Payments Processor"] !== undefined) payProcessor += 1;
                        if (jsonObj["Payment"]["Payment Acceptance"] !== undefined) payAcceptance += 1;
                        if (jsonObj["Payment"]["Payment Currency"] !== undefined) payCurrency += 1;
                        if (jsonObj["Payment"]["Checkout Buttons"] !== undefined) checkOurButton += 1;

                        if (jsonObj["Payment"]["Payment Acceptance"] !== undefined &&
                            jsonObj["Payment"]["Payment Currency"] !== undefined &&
                            jsonObj["Payment"]["Checkout Buttons"] !== undefined
                        ) {
                            PAPCCO += 1;
                        }

                        if (jsonObj["Payment"]["Payments Processor"] !== undefined &&
                            jsonObj["Payment"]["Payment Acceptance"] !== undefined &&
                            jsonObj["Payment"]["Payment Currency"] !== undefined &&
                            jsonObj["Payment"]["Checkout Buttons"] !== undefined
                        ) {
                            PPPAPCCO += 1;
                        }

                    }

                    if (jsonObj["Widgets"] !== undefined) {
                        totalWidgetsCount += 1;
                        if (jsonObj["Widgets"]["Live Chat"] !== undefined) liveChat += 1;
                        if (jsonObj["Widgets"]["Login"] !== undefined) customerLogin += 1;
                        if (jsonObj["Widgets"]["Social Sharing"] !== undefined) socialSharing += 1;
                        if (jsonObj["Widgets"]["Schedule Management"] !== undefined) scheduleManagement += 1;
                        if (jsonObj["Widgets"]["Ticketing System"] !== undefined) ticketingSystem += 1;
                        if (jsonObj["Widgets"]["Bookings"] !== undefined) bookings += 1;
                    }
                }
            }

            let adsObj = {};
            adsObj["Total Company"] = totalAdvertisingCount;
            adsObj["Country"] = datasets[i];
            adsObj["Ad Network"] = isNaN(Math.round((adNetwork / totalAdvertisingCount) * 100)) ? 0 : Math.round((adNetwork / totalAdvertisingCount) * 100);
            adsObj["ads txt"] = isNaN(Math.round((adsTxt / totalAdvertisingCount) * 100)) ? 0 : Math.round((adsTxt / totalAdvertisingCount) * 100);
            adsObj["Contextual Advertising"] = isNaN(Math.round((contextualAds / totalAdvertisingCount) * 100)) ? 0 : Math.round((contextualAds / totalAdvertisingCount) * 100);
            adsObj["Ad Exchange"] = isNaN(Math.round((adExchange / totalAdvertisingCount) * 100)) ? 0 : Math.round((adExchange / totalAdvertisingCount) * 100);
            adsObj["Facebook Exchange"] = isNaN(Math.round((fbExchange / totalAdvertisingCount) * 100)) ? 0 : Math.round((fbExchange / totalAdvertisingCount) * 100);
            adsObj["Retargeting / Remarketing"] = isNaN(Math.round((retargetingAds / totalAdvertisingCount) * 100)) ? 0 : Math.round((retargetingAds / totalAdvertisingCount) * 100);

            let analyticAndTrackingObj = {};
            analyticAndTrackingObj["Total Company"] = totalAnalyticsCount;
            analyticAndTrackingObj["Country"] = datasets[i];
            analyticAndTrackingObj["Conversion Optimization"] = isNaN(Math.round((conversionOpt / totalAnalyticsCount) * 100)) ? 0 : Math.round((conversionOpt / totalAnalyticsCount) * 100);
            analyticAndTrackingObj["Tag Management"] = isNaN(Math.round((tagMng / totalAnalyticsCount) * 100)) ? 0 : Math.round((tagMng / totalAnalyticsCount) * 100);
            analyticAndTrackingObj["Advertiser Tracking"] = isNaN(Math.round((adsTracking / totalAnalyticsCount) * 100)) ? 0 : Math.round((adsTracking / totalAnalyticsCount) * 100);
            analyticAndTrackingObj["Audience Measurement"] = isNaN(Math.round((audienceMeasurement / totalAnalyticsCount) * 100)) ? 0 : Math.round((audienceMeasurement / totalAnalyticsCount) * 100);
            analyticAndTrackingObj["Lead Generation"] = isNaN(Math.round((leadGeneration / totalAnalyticsCount) * 100)) ? 0 : Math.round((leadGeneration / totalAnalyticsCount) * 100);
            analyticAndTrackingObj["Marketing Automation"] = isNaN(Math.round((marketAutomation / totalAnalyticsCount) * 100)) ? 0 : Math.round((marketAutomation / totalAnalyticsCount) * 100);

            let ecommerceObj = {};
            ecommerceObj["Total Company"] = totalEcommerceCount;
            ecommerceObj["Country"] = datasets[i];
            ecommerceObj["Non Platform"] = isNaN(Math.round((nonPlatform / totalEcommerceCount) * 100)) ? 0 : Math.round((nonPlatform / totalEcommerceCount) * 100);
            ecommerceObj["Hosted Solution"] = isNaN(Math.round((hostedSolution / totalEcommerceCount) * 100)) ? 0 : Math.round((hostedSolution / totalEcommerceCount) * 100);
            ecommerceObj["Open Source"] = isNaN(Math.round((openSource / totalEcommerceCount) * 100)) ? 0 : Math.round((openSource / totalEcommerceCount) * 100);
            ecommerceObj["Multi-Channel"] = isNaN(Math.round((multiChannel / totalEcommerceCount) * 100)) ? 0 : Math.round((multiChannel / totalEcommerceCount) * 100);
            ecommerceObj["Enterprise"] = isNaN(Math.round((enterprise / totalEcommerceCount) * 100)) ? 0 : Math.round((enterprise / totalEcommerceCount) * 100);
            ecommerceObj["SMB Solution"] = isNaN(Math.round((smbSolution / totalEcommerceCount) * 100)) ? 0 : Math.round((smbSolution / totalEcommerceCount) * 100);

            let paymentObj = {};
            paymentObj["Total Company"] = totalPaymentCount;
            paymentObj["Country"] = datasets[i];
            paymentObj["Payments Processor"] = isNaN(Math.round((payProcessor / totalPaymentCount) * 100)) ? 0 : Math.round((payProcessor / totalPaymentCount) * 100);
            paymentObj["Payment Acceptance"] = isNaN(Math.round((payAcceptance / totalPaymentCount) * 100)) ? 0 : Math.round((payAcceptance / totalPaymentCount) * 100);
            paymentObj["Payment Currency"] = isNaN(Math.round((payCurrency / totalPaymentCount) * 100)) ? 0 : Math.round((payCurrency / totalPaymentCount) * 100);
            paymentObj["Checkout Buttons"] = isNaN(Math.round((checkOurButton / totalPaymentCount) * 100)) ? 0 : Math.round((checkOurButton / totalPaymentCount) * 100);
            paymentObj["PP, PA, PC, CO"] = isNaN(Math.round((PPPAPCCO / totalPaymentCount) * 100)) ? 0 : Math.round((PPPAPCCO / totalPaymentCount) * 100);
            paymentObj["PA & PC CO"] = isNaN(Math.round((PAPCCO / totalPaymentCount) * 100)) ? 0 : Math.round((PAPCCO / totalPaymentCount) * 100);

            let widgetsObj = {};
            widgetsObj["Total Company"] = totalWidgetsCount;
            widgetsObj["Country"] = datasets[i];
            widgetsObj["Live Chat"] = isNaN(Math.round((liveChat / totalWidgetsCount) * 100)) ? 0 : Math.round((liveChat / totalWidgetsCount) * 100);
            widgetsObj["Customer Login"] = isNaN(Math.round((customerLogin / totalWidgetsCount) * 100)) ? 0 : Math.round((customerLogin / totalWidgetsCount) * 100);
            widgetsObj["Social Sharing"] = isNaN(Math.round((socialSharing / totalWidgetsCount) * 100)) ? 0 : Math.round((socialSharing / totalWidgetsCount) * 100);
            widgetsObj["Schedule Management"] = isNaN(Math.round((scheduleManagement / totalWidgetsCount) * 100)) ? 0 : Math.round((scheduleManagement / totalWidgetsCount) * 100);
            widgetsObj["Ticketing System"] = isNaN(Math.round((ticketingSystem / totalWidgetsCount) * 100)) ? 0 : Math.round((ticketingSystem / totalWidgetsCount) * 100);
            widgetsObj["Bookings"] = isNaN(Math.round((bookings / totalWidgetsCount) * 100)) ? 0 : Math.round((bookings / totalWidgetsCount) * 100);

            let allTechnologyObj = {};
            allTechnologyObj["Total Company"] = companies.length;
            allTechnologyObj["Country"] = datasets[i];
            allTechnologyObj["Advertising"] = isNaN(Math.round((totalAdvertisingCount / companies.length) * 100)) ? 0 : Math.round((totalAdvertisingCount / companies.length) * 100);
            allTechnologyObj["SEO"] = 0;
            allTechnologyObj["Analytics"] = isNaN(Math.round((totalAnalyticsCount / companies.length) * 100)) ? 0 : Math.round((totalAnalyticsCount / companies.length) * 100);
            allTechnologyObj["E-commerce"] = isNaN(Math.round((totalEcommerceCount / companies.length) * 100)) ? 0 : Math.round((totalEcommerceCount / companies.length) * 100);
            allTechnologyObj["Payments"] = isNaN(Math.round((totalPaymentCount / companies.length) * 100)) ? 0 : Math.round((totalPaymentCount / companies.length) * 100);
            allTechnologyObj["Widgets"] = isNaN(Math.round((totalWidgetsCount / companies.length) * 100)) ? 0 : Math.round((totalWidgetsCount / companies.length) * 100);

            data["All Technology"].push(allTechnologyObj);
            data["Advertising"].push(adsObj);
            data["Analytics and Tracking"].push(analyticAndTrackingObj);
            data["Ecommerce"].push(ecommerceObj);
            data["Payment"].push(paymentObj);
            data["Widgets"].push(widgetsObj);
        }

        return res.status(200).json({
            message: "Successful",
            results: data,
            technologies: technologies,
            header: header
        });
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

exports.getProviderTechnology = async function (req, res) {

    const datasets = req.body.datasets;
    const filename = req.body.fileName;

    let results = [];

    let types = ["Advertising", "Analytics and Tracking", "Ecommerce", "Payment", "Widgets"];

    let headers = [...datasets];

    const categoryTypes = {
        "Advertising": [
            // "Avertising  Network",
            "ads txt",
            // "Ad Exchange",
            "Audience Targeting",
            // "Facebook Exchange",
            // "Ad Server",
            // "Affiliate Programs",
            "Contextual Advertising",
            "Dynamic Creative Optimization",
            "Digital Video Ads",
            "Retargeting / Remarketing",
            // "Header Bidding"
        ],
        "Analytics and Tracking":
            [
                "Application Performance",
                // "A/B Testing",
                // "Ad Analytics",
                "Conversion Optimization",
                "Advertiser Tracking",
                "Tag Management",
                "Audience Measurement",
                "Visitor Count Tracking"
            ],
        "Ecommerce":
            [
                "Non Platform",
                "Hosted Solution",
                "Open Source",
                "Checkout Buttons",
                "Payment Acceptance",
                "Payments Processor",
                // "Payment Currency"
            ],
        "Widget":
            [
                "Live Chat",
                "Login",
                "Ticketing System",
                "Bookings",
                "Social Sharing",
                "Social Management"
            ],
        "Hosting":
            [
                "Cloud Hosting",
                "Cloud PaaS",
                "Dedicated Hostig",
                "Business Email Hosting",
                "Web Hosting Provider Email",
                "Marketing Platform",
            ],
        "Productivity":
            [
                "CRM",
                "Lead Generation",
                "Marketing Automation",
                "Product Recommendations",
                "Feedback Forms and Surveys",
                "Campaign Management"
            ]
    };

    let total = {
        "Advertising": {},
        "Analytics and Tracking": {},
        "Ecommerce": {},
        "Payment": {},
        "Widgets": {}
    }

    try {
        for (let i = 0; i < datasets.length; i++) {

            let data = {
                "Advertising": {
                    "ads txt": {},
                    "Audience Targeting": {},
                    "Contextual Advertising": {},
                    "Dynamic Creative Optimization": {},
                    "Digital Video Ads": {},
                    "Retargeting / Remarketing": {},
                },
                "Analytics and Tracking": {
                    "Application Performance": {},
                    "Conversion Optimization": {},
                    "Advertiser Tracking": {},
                    "Tag Management": {},
                    "Audience Measurement": {},
                    "Visitor Count Tracking": {},
                },
                "Ecommerce": {
                    "Non Platform": {},
                    "Hosted Solution": {},
                    "Open Source": {},
                    "Checkout Buttons": {},
                    "Payment Acceptance": {},
                    "Payments Processor": {},
                },
                "Hosting": {
                    "Cloud Hosting": {},
                    "Cloud PaaS": {},
                    "Dedicated Hostig": {},
                    "Business Email Hosting": {},
                    "Web Hosting Provider Email": {},
                    "Marketing Platform": {},
                },
                "Widgets": {
                    "Live Chat": {},
                    "Login": {},
                    "Ticketing System": {},
                    "Bookings": {},
                    "Social Sharing": {},
                    "Social Management": {},
                },
                "Productivity": {
                    "CRM": {},
                    "Lead Generation": {},
                    "Marketing Automation": {},
                    "Product Recommendations": {},
                    "Feedback Forms and Surveys": {},
                    "Campaign Management": {},
                }
            };

            let companies;
            if (filename !== "Master DB (Golden Source)") {
                companies = await CompanyItem.findAll({
                    attributes: ["asset"],
                    where: {
                        [Op.and]: [
                            { dataset: datasets[i] },
                            {
                                asset: {
                                    [Op.ne]: null
                                }
                            },
                            {
                                file_name: filename
                            }
                        ]
                    }
                });
            } else {
                companies = await CompanyItem.findAll({
                    attributes: ["asset"],
                    where: {
                        [Op.and]: [
                            { dataset: datasets[i] },
                            {
                                asset: {
                                    [Op.ne]: null
                                }
                            }
                        ]
                    }
                });
            }

            for (let j = 0; j < companies.length; j++) {

                let jsonObj = JSON.parse(companies[j]["asset"]);

                if (jsonObj["Advertising"] !== undefined) {
                    if (total["Advertising"][datasets[i]] === undefined) {
                        total["Advertising"][datasets[i]] = { count: 1 };
                    } else {
                        total["Advertising"][datasets[i]]["count"] += 1;
                    }
                    if (jsonObj["Advertising"]["Ad Network"] !== undefined) {
                        (jsonObj["Advertising"]["Ad Network"]).map(a => {
                            if (data["Advertising"]["Ad Network"][a] === undefined) {
                                data["Advertising"]["Ad Network"][a] = { "name": a, "count": 0 };
                            }
                            data["Advertising"]["Ad Network"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Advertising"]["ads txt"] !== undefined) {
                        (jsonObj["Advertising"]["ads txt"]).map(a => {
                            if (data["Advertising"]["ads txt"][a] === undefined) {
                                data["Advertising"]["ads txt"][a] = { "name": a, "count": 0 };
                            }
                            data["Advertising"]["ads txt"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Advertising"]["Contextual Advertising"] !== undefined) {
                        (jsonObj["Advertising"]["Contextual Advertising"]).map(a => {
                            if (data["Advertising"]["Contextual Advertising"][a] === undefined) {
                                data["Advertising"]["Contextual Advertising"][a] = { "name": a, "count": 0 };
                            }
                            data["Advertising"]["Contextual Advertising"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Advertising"]["Ad Exchange"] !== undefined) {
                        (jsonObj["Advertising"]["Ad Exchange"]).map(a => {
                            if (data["Advertising"]["Ad Exchange"][a] === undefined) {
                                data["Advertising"]["Ad Exchange"][a] = { "name": a, "count": 0 };
                            }
                            data["Advertising"]["Ad Exchange"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Advertising"]["Facebook Exchange"] !== undefined) {
                        (jsonObj["Advertising"]["Facebook Exchange"]).map(a => {
                            if (data["Advertising"]["Facebook Exchange"][a] === undefined) {
                                data["Advertising"]["Facebook Exchange"][a] = { "name": a, "count": 0 };
                            }
                            data["Advertising"]["Facebook Exchange"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Advertising"]["Retargeting / Remarketing"] !== undefined) {
                        (jsonObj["Advertising"]["Retargeting / Remarketing"]).map(a => {
                            if (data["Advertising"]["Retargeting / Remarketing"][a] === undefined) {
                                data["Advertising"]["Retargeting / Remarketing"][a] = { "name": a, "count": 0 };
                            }
                            data["Advertising"]["Retargeting / Remarketing"][a]["count"] += 1;
                        });
                    }
                }

                if (jsonObj["Analytics and Tracking"] !== undefined) {
                    if (total["Analytics and Tracking"][datasets[i]] === undefined) {
                        total["Analytics and Tracking"][datasets[i]] = { count: 1 };
                    } else {
                        total["Analytics and Tracking"][datasets[i]]["count"] += 1;
                    }
                    if (jsonObj["Analytics and Tracking"]["Conversion Optimization"] !== undefined) {
                        (jsonObj["Analytics and Tracking"]["Conversion Optimization"]).map(a => {
                            if (data["Analytics and Tracking"]["Conversion Optimization"][a] === undefined) {
                                data["Analytics and Tracking"]["Conversion Optimization"][a] = { "name": a, "count": 0 };
                            }
                            data["Analytics and Tracking"]["Conversion Optimization"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Analytics and Tracking"]["Tag Management"] !== undefined) {
                        (jsonObj["Analytics and Tracking"]["Tag Management"]).map(a => {
                            if (data["Analytics and Tracking"]["Tag Management"][a] === undefined) {
                                data["Analytics and Tracking"]["Tag Management"][a] = { "name": a, "count": 0 };
                            }
                            data["Analytics and Tracking"]["Tag Management"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Analytics and Tracking"]["Advertiser Tracking"] !== undefined) {
                        (jsonObj["Analytics and Tracking"]["Advertiser Tracking"]).map(a => {
                            if (data["Analytics and Tracking"]["Advertiser Tracking"][a] === undefined) {
                                data["Analytics and Tracking"]["Advertiser Tracking"][a] = { "name": a, "count": 0 };
                            }
                            data["Analytics and Tracking"]["Advertiser Tracking"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Analytics and Tracking"]["Audience Measurement"] !== undefined) {
                        (jsonObj["Analytics and Tracking"]["Audience Measurement"]).map(a => {
                            if (data["Analytics and Tracking"]["Audience Measurement"][a] === undefined) {
                                data["Analytics and Tracking"]["Audience Measurement"][a] = { "name": a, "count": 0 };
                            }
                            data["Analytics and Tracking"]["Audience Measurement"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Analytics and Tracking"]["Lead Generation"] !== undefined) {
                        (jsonObj["Analytics and Tracking"]["Lead Generation"]).map(a => {
                            if (data["Analytics and Tracking"]["Lead Generation"][a] === undefined) {
                                data["Analytics and Tracking"]["Lead Generation"][a] = { "name": a, "count": 0 };
                            }
                            data["Analytics and Tracking"]["Lead Generation"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Analytics and Tracking"]["Marketing Automation"] !== undefined) {
                        (jsonObj["Analytics and Tracking"]["Marketing Automation"]).map(a => {
                            if (data["Analytics and Tracking"]["Marketing Automation"][a] === undefined) {
                                data["Analytics and Tracking"]["Marketing Automation"][a] = { "name": a, "count": 0 };
                            }
                            data["Analytics and Tracking"]["Marketing Automation"][a]["count"] += 1;
                        });
                    }
                }

                if (jsonObj["Ecommerce"] !== undefined) {
                    if (total["Ecommerce"][datasets[i]] === undefined) {
                        total["Ecommerce"][datasets[i]] = { count: 1 };
                    } else {
                        total["Ecommerce"][datasets[i]]["count"] += 1;
                    }
                    if (jsonObj["Ecommerce"]["Non Platform"] !== undefined) {
                        (jsonObj["Ecommerce"]["Non Platform"]).map(a => {
                            if (data["Ecommerce"]["Non Platform"][a] === undefined) {
                                data["Ecommerce"]["Non Platform"][a] = { "name": a, "count": 0 };
                            }
                            data["Ecommerce"]["Non Platform"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Ecommerce"]["Hosted Solution"] !== undefined) {
                        (jsonObj["Ecommerce"]["Hosted Solution"]).map(a => {
                            if (data["Ecommerce"]["Hosted Solution"][a] === undefined) {
                                data["Ecommerce"]["Hosted Solution"][a] = { "name": a, "count": 0 };
                            }
                            data["Ecommerce"]["Hosted Solution"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Ecommerce"]["Open Source"] !== undefined) {
                        (jsonObj["Ecommerce"]["Open Source"]).map(a => {
                            if (data["Ecommerce"]["Open Source"][a] === undefined) {
                                data["Ecommerce"]["Open Source"][a] = { "name": a, "count": 0 };
                            }
                            data["Ecommerce"]["Open Source"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Ecommerce"]["Multi-Channel"] !== undefined) {
                        (jsonObj["Ecommerce"]["Multi-Channel"]).map(a => {
                            if (data["Ecommerce"]["Multi-Channel"][a] === undefined) {
                                data["Ecommerce"]["Multi-Channel"][a] = { "name": a, "count": 0 };
                            }
                            data["Ecommerce"]["Multi-Channel"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Ecommerce"]["Enterprise"] !== undefined) {
                        (jsonObj["Ecommerce"]["Enterprise"]).map(a => {
                            if (data["Ecommerce"]["Enterprise"][a] === undefined) {
                                data["Ecommerce"]["Enterprise"][a] = { "name": a, "count": 0 };
                            }
                            data["Ecommerce"]["Enterprise"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Ecommerce"]["SMB Solution"] !== undefined) {
                        (jsonObj["Ecommerce"]["SMB Solution"]).map(a => {
                            if (data["Ecommerce"]["SMB Solution"][a] === undefined) {
                                data["Ecommerce"]["SMB Solution"][a] = { "name": a, "count": 0 };
                            }
                            data["Ecommerce"]["SMB Solution"][a]["count"] += 1;
                        });
                    }
                }

                if (jsonObj["Payment"] !== undefined) {
                    if (total["Payment"][datasets[i]] === undefined) {
                        total["Payment"][datasets[i]] = { count: 1 };
                    } else {
                        total["Payment"][datasets[i]]["count"] += 1;
                    }
                    if (jsonObj["Payment"]["Payments Processor"] !== undefined) {
                        (jsonObj["Payment"]["Payments Processor"]).map(a => {
                            if (data["Payment"]["Payments Processor"][a] === undefined) {
                                data["Payment"]["Payments Processor"][a] = { "name": a, "count": 0 };
                            }
                            data["Payment"]["Payments Processor"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Payment"]["Payment Acceptance"] !== undefined) {
                        (jsonObj["Payment"]["Payment Acceptance"]).map(a => {
                            if (data["Payment"]["Payment Acceptance"][a] === undefined) {
                                data["Payment"]["Payment Acceptance"][a] = { "name": a, "count": 0 };
                            }
                            data["Payment"]["Payment Acceptance"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Payment"]["Payment Currency"] !== undefined) {
                        (jsonObj["Payment"]["Payment Currency"]).map(a => {
                            if (data["Payment"]["Payment Currency"][a] === undefined) {
                                data["Payment"]["Payment Currency"][a] = { "name": a, "count": 0 };
                            }
                            data["Payment"]["Payment Currency"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Payment"]["Checkout Buttons"] !== undefined) {
                        (jsonObj["Payment"]["Checkout Buttons"]).map(a => {
                            if (data["Payment"]["Checkout Buttons"][a] === undefined) {
                                data["Payment"]["Checkout Buttons"][a] = { "name": a, "count": 0 };
                            }
                            data["Payment"]["Checkout Buttons"][a]["count"] += 1;
                        });
                    }
                }

                if (jsonObj["Widgets"] !== undefined) {
                    if (total["Widgets"][datasets[i]] === undefined) {
                        total["Widgets"][datasets[i]] = { count: 1 };
                    } else {
                        total["Widgets"][datasets[i]]["count"] += 1;
                    }
                    if (jsonObj["Widgets"]["Live Chat"] !== undefined) {
                        (jsonObj["Widgets"]["Live Chat"]).map(a => {
                            if (data["Widgets"]["Live Chat"][a] === undefined) {
                                data["Widgets"]["Live Chat"][a] = { "name": a, "count": 0 };
                            }
                            data["Widgets"]["Live Chat"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Widgets"]["Customer Login"] !== undefined) {
                        (jsonObj["Widgets"]["Customer Login"]).map(a => {
                            if (data["Widgets"]["Customer Login"][a] === undefined) {
                                data["Widgets"]["Customer Login"][a] = { "name": a, "count": 0 };
                            }
                            data["Widgets"]["Customer Login"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Widgets"]["Social Sharing"] !== undefined) {
                        (jsonObj["Widgets"]["Social Sharing"]).map(a => {
                            if (data["Widgets"]["Social Sharing"][a] === undefined) {
                                data["Widgets"]["Social Sharing"][a] = { "name": a, "count": 0 };
                            }
                            data["Widgets"]["Social Sharing"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Widgets"]["Schedule Management"] !== undefined) {
                        (jsonObj["Widgets"]["Schedule Management"]).map(a => {
                            if (data["Widgets"]["Schedule Management"][a] === undefined) {
                                data["Widgets"]["Schedule Management"][a] = { "name": a, "count": 0 };
                            }
                            data["Widgets"]["Schedule Management"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Widgets"]["Ticketing System"] !== undefined) {
                        (jsonObj["Widgets"]["Ticketing System"]).map(a => {
                            if (data["Widgets"]["Ticketing System"][a] === undefined) {
                                data["Widgets"]["Ticketing System"][a] = { "name": a, "count": 0 };
                            }
                            data["Widgets"]["Ticketing System"][a]["count"] += 1;
                        });
                    }
                    if (jsonObj["Widgets"]["Bookings"] !== undefined) {
                        (jsonObj["Widgets"]["Bookings"]).map(a => {
                            if (data["Widgets"]["Bookings"][a] === undefined) {
                                data["Widgets"]["Bookings"][a] = { "name": a, "count": 0 };
                            }
                            data["Widgets"]["Bookings"][a]["count"] += 1;
                        });
                    }
                }
            }
            results.push(data);
        }

        return res.status(200).json({
            message: "Successful",
            results: results,
            types: types,
            categoryTypes: categoryTypes,
            headers: headers,
            total: total
        });

    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

exports.getTechnologyCountryView = async function (req, res) {

    const datasets = req.body.datasets;
    const industries = req.body.industries;
    const filename = req.body.fileName;

    let results = [];
    let jsonObj = {};
    let industryObj = {};
    let indObj = {};
    let data = {};

    try {

        for (let i = 0; i < datasets.length; i++) {

            let companies;
            if (filename !== "Master DB (Golden Source)") {
                companies = await CompanyItem.findAll({
                    attributes: ["industry", "asset"],
                    where: {
                        [Op.and]: [
                            { dataset: datasets[i] },
                            {
                                industry: {
                                    [Op.in]: industries
                                }
                            },
                            {
                                asset: {
                                    [Op.ne]: null
                                }
                            },
                            {
                                file_name: filename
                            }
                        ]
                    }
                });
            } else {
                companies = await CompanyItem.findAll({
                    attributes: ["industry", "asset"],
                    where: {
                        [Op.and]: [
                            { dataset: datasets[i] },
                            {
                                industry: {
                                    [Op.in]: industries
                                }
                            },
                            {
                                asset: {
                                    [Op.ne]: null
                                }
                            }
                        ]
                    }
                });
            }


            data[datasets[i]] = {
                "Advertising": {
                    "Ad Network": {},
                    "ads txt": {},
                    "Contextual Advertising": {},
                    "Ad Exchange": {},
                    "Facebook Exchange": {},
                    "Retargeting / Remarketing": {}
                },
                "Analytics": {
                    "Conversion Optimization": {},
                    "Tag Management": {},
                    "Advertiser Tracking": {},
                    "Audience Measurement": {},
                    "Lead Generation": {},
                    "Marketing Automation": {}
                },
                "Ecommerce": {
                    "Non Platform": {},
                    "Hosted Solution": {},
                    "Open Source": {},
                    "Multi-Channel": {},
                    "Enterprise": {},
                    "SMB Solution": {}
                },
                "Payment": {
                    "Payments Processor": {},
                    "Payment Acceptance": {},
                    "Payment Currency": {},
                    "Checkout Buttons": {}
                },
                "Widgets": {
                    "Live Chat": {},
                    "Customer Login": {},
                    "Social Sharing": {},
                    "Schedule Management": {},
                    "Ticketing System": {},
                    "Bookings": {}
                }
            }

            let adNetwork = 0;
            let adsTxt = 0;
            let contextualAds = 0;
            let adExchange = 0;
            let fbExchange = 0;
            let retargetingAds = 0;

            let conversionOpt = 0;
            let tagMng = 0;
            let adsTracking = 0;
            let audienceMeasurement = 0;
            let leadGeneration = 0;
            let marketAutomation = 0;

            let nonPlatform = 0;
            let hostedSolution = 0;
            let openSource = 0;
            let multiChannel = 0;
            let enterprise = 0;
            let smbSolution = 0;

            let payProcessor = 0;
            let payAcceptance = 0;
            let payCurrency = 0;
            let checkOurButton = 0;
            let PPPAPCCO = 0;
            let PAPCCO = 0;

            let liveChat = 0;
            let customerLogin = 0;
            let socialSharing = 0;
            let scheduleManagement = 0;
            let ticketingSystem = 0;
            let bookings = 0;

            let totalAdvertisingCount = 0;
            let totalSEOCount = 0;
            let totalAnalyticsCount = 0;
            let totalEcommerceCount = 0;
            let totalPaymentCount = 0;
            let totalWidgetsCount = 0;

            if (jsonObj[datasets[i]] === undefined) {
                jsonObj[datasets[i]] = {
                    "Advertising": {},
                    "Analytics": {},
                    "Ecommerce": {},
                    "Payment": {},
                    "Widgets": {}
                };
            }

            if (industryObj[datasets[i]] === undefined) industryObj[datasets[i]] = {};
            if (industryObj[datasets[i]]["Advertising"] === undefined) industryObj[datasets[i]]["Advertising"] = {};
            if (industryObj[datasets[i]]["Analytics"] === undefined) industryObj[datasets[i]]["Analytics"] = {};
            if (industryObj[datasets[i]]["Ecommerce"] === undefined) industryObj[datasets[i]]["Ecommerce"] = {};
            if (industryObj[datasets[i]]["Payment"] === undefined) industryObj[datasets[i]]["Payment"] = {};
            if (industryObj[datasets[i]]["Widgets"] === undefined) industryObj[datasets[i]]["Widgets"] = {};

            for (let j = 0; j < companies.length; j++) {

                industryObj[datasets[i]]["Advertising"][companies[j].industry] = [];
                industryObj[datasets[i]]["Analytics"][companies[j].industry] = [];
                industryObj[datasets[i]]["Ecommerce"][companies[j].industry] = [];
                industryObj[datasets[i]]["Payment"][companies[j].industry] = [];
                industryObj[datasets[i]]["Widgets"][companies[j].industry] = [];

                let assets = JSON.parse(companies[j]["asset"]);

                if (assets) {

                    indObj[companies[j].industry] === undefined ? indObj[companies[j].industry] = {} : null;

                    if (assets["Advertising"] !== undefined) {
                        if (assets["Advertising"]["Ad Network"] !== undefined) {
                            adNetwork += 1;
                            if (indObj[companies[j].industry]["Ad Network"] === undefined) indObj[companies[j].industry]["Ad Network"] = 1
                            else indObj[companies[j].industry]["Ad Network"] += 1;
                            (assets["Advertising"]["Ad Network"]).map(a => {
                                if (data[datasets[i]]["Advertising"]["Ad Network"][a] === undefined) {
                                    data[datasets[i]]["Advertising"]["Ad Network"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Advertising"]["Ad Network"][a]["count"] += 1;
                            });
                        }
                        if (assets["Advertising"]["ads txt"] !== undefined) {
                            adsTxt += 1;
                            if (indObj[companies[j].industry]["ads txt"] === undefined) indObj[companies[j].industry]["ads txt"] = 1
                            else indObj[companies[j].industry]["ads txt"] += 1;
                            (assets["Advertising"]["ads txt"]).map(a => {
                                if (data[datasets[i]]["Advertising"]["ads txt"][a] === undefined) {
                                    data[datasets[i]]["Advertising"]["ads txt"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Advertising"]["ads txt"][a]["count"] += 1;
                            });
                        }
                        if (assets["Advertising"]["Contextual Advertising"] !== undefined) {
                            contextualAds += 1;
                            if (indObj[companies[j].industry]["Contextual Advertising"] === undefined) indObj[companies[j].industry]["Contextual Advertising"] = 1
                            else indObj[companies[j].industry]["Contextual Advertising"] += 1;
                            (assets["Advertising"]["Contextual Advertising"]).map(a => {
                                if (data[datasets[i]]["Advertising"]["Contextual Advertising"][a] === undefined) {
                                    data[datasets[i]]["Advertising"]["Contextual Advertising"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Advertising"]["Contextual Advertising"][a]["count"] += 1;
                            });
                        }
                        if (assets["Advertising"]["Ad Exchange"] !== undefined) {
                            adExchange += 1;
                            if (indObj[companies[j].industry]["Ad Exchange"] === undefined) indObj[companies[j].industry]["Ad Exchange"] = 1
                            else indObj[companies[j].industry]["Ad Exchange"] += 1;
                            (assets["Advertising"]["Ad Exchange"]).map(a => {
                                if (data[datasets[i]]["Advertising"]["Ad Exchange"][a] === undefined) {
                                    data[datasets[i]]["Advertising"]["Ad Exchange"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Advertising"]["Ad Exchange"][a]["count"] += 1;
                            });
                        }
                        if (assets["Advertising"]["Facebook Exchange"] !== undefined) {
                            fbExchange += 1;
                            if (indObj[companies[j].industry]["Facebook Exchange"] === undefined) indObj[companies[j].industry]["Facebook Exchange"] = 1
                            else indObj[companies[j].industry]["Facebook Exchange"] += 1;
                            (assets["Advertising"]["Facebook Exchange"]).map(a => {
                                if (data[datasets[i]]["Advertising"]["Facebook Exchange"][a] === undefined) {
                                    data[datasets[i]]["Advertising"]["Facebook Exchange"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Advertising"]["Facebook Exchange"][a]["count"] += 1;
                            });
                        }
                        if (assets["Advertising"]["Retargeting / Remarketing"] !== undefined) {
                            retargetingAds += 1;
                            if (indObj[companies[j].industry]["Retargeting / Remarketing"] === undefined) indObj[companies[j].industry]["Retargeting / Remarketing"] = 1
                            else indObj[companies[j].industry]["Retargeting / Remarketing"] += 1;
                            (assets["Advertising"]["Retargeting / Remarketing"]).map(a => {
                                if (data[datasets[i]]["Advertising"]["Retargeting / Remarketing"][a] === undefined) {
                                    data[datasets[i]]["Advertising"]["Retargeting / Remarketing"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Advertising"]["Retargeting / Remarketing"][a]["count"] += 1;
                            });
                        }
                        totalAdvertisingCount += 1;
                    }

                    if (assets["Analytics and Tracking"] !== undefined) {
                        if (assets["Analytics and Tracking"]["Conversion Optimization"] !== undefined) {
                            conversionOpt += 1;
                            if (indObj[companies[j].industry]["Conversion Optimization"] === undefined) indObj[companies[j].industry]["Conversion Optimization"] = 1
                            else indObj[companies[j].industry]["Conversion Optimization"] += 1;
                            (assets["Analytics and Tracking"]["Conversion Optimization"]).map(a => {
                                if (data[datasets[i]]["Analytics"]["Conversion Optimization"][a] === undefined) {
                                    data[datasets[i]]["Analytics"]["Conversion Optimization"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Analytics"]["Conversion Optimization"][a]["count"] += 1;
                            });
                        }
                        if (assets["Analytics and Tracking"]["Tag Management"] !== undefined) {
                            tagMng += 1;
                            if (indObj[companies[j].industry]["Tag Management"] === undefined) indObj[companies[j].industry]["Tag Management"] = 1
                            else indObj[companies[j].industry]["Tag Management"] += 1;
                            let arr = assets["Analytics and Tracking"]["Tag Management"];

                            arr.map(a => {
                                if (data[datasets[i]]["Analytics"]["Tag Management"][a] === undefined) {
                                    data[datasets[i]]["Analytics"]["Tag Management"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Analytics"]["Tag Management"][a]["count"] += 1;
                            });
                        }
                        if (assets["Analytics and Tracking"]["Advertiser Tracking"] !== undefined) {
                            adsTracking += 1;
                            if (indObj[companies[j].industry]["Advertiser Tracking"] === undefined) indObj[companies[j].industry]["Advertiser Tracking"] = 1
                            else indObj[companies[j].industry]["Advertiser Tracking"] += 1;
                            (assets["Analytics and Tracking"]["Advertiser Tracking"]).map(a => {
                                if (data[datasets[i]]["Analytics"]["Advertiser Tracking"][a] === undefined) {
                                    data[datasets[i]]["Analytics"]["Advertiser Tracking"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Analytics"]["Advertiser Tracking"][a]["count"] += 1;
                            });
                        }
                        if (assets["Analytics and Tracking"]["Audience Measurement"] !== undefined) {
                            audienceMeasurement += 1;
                            if (indObj[companies[j].industry]["Audience Measurement"] === undefined) indObj[companies[j].industry]["Audience Measurement"] = 1
                            else indObj[companies[j].industry]["Audience Measurement"] += 1;
                            (assets["Analytics and Tracking"]["Audience Measurement"]).map(a => {
                                if (data[datasets[i]]["Analytics"]["Audience Measurement"][a] === undefined) {
                                    data[datasets[i]]["Analytics"]["Audience Measurement"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Analytics"]["Audience Measurement"][a]["count"] += 1;
                            });
                        }
                        if (assets["Analytics and Tracking"]["Lead Generation"] !== undefined) {
                            leadGeneration += 1;
                            if (indObj[companies[j].industry]["Lead Generation"] === undefined) indObj[companies[j].industry]["Lead Generation"] = 1
                            else indObj[companies[j].industry]["Lead Generation"] += 1;
                            (assets["Analytics and Tracking"]["Lead Generation"]).map(a => {
                                if (data[datasets[i]]["Analytics"]["Lead Generation"][a] === undefined) {
                                    data[datasets[i]]["Analytics"]["Lead Generation"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Analytics"]["Lead Generation"][a]["count"] += 1;
                            });
                        }
                        if (assets["Analytics and Tracking"]["Marketing Automation"] !== undefined) {
                            marketAutomation += 1;
                            if (indObj[companies[j].industry]["Marketing Automation"] === undefined) indObj[companies[j].industry]["Marketing Automation"] = 1
                            else indObj[companies[j].industry]["Marketing Automation"] += 1;
                            (assets["Analytics and Tracking"]["Marketing Automation"]).map(a => {
                                if (data[datasets[i]]["Analytics"]["Marketing Automation"][a] === undefined) {
                                    data[datasets[i]]["Analytics"]["Marketing Automation"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Analytics"]["Marketing Automation"][a]["count"] += 1;
                            });
                        }
                        totalAnalyticsCount += 1;
                    }

                    if (assets["Ecommerce"] !== undefined) {
                        if (assets["Ecommerce"]["Non Platform"] !== undefined) {
                            nonPlatform += 1;
                            if (indObj[companies[j].industry]["Non Platform"] === undefined) indObj[companies[j].industry]["Non Platform"] = 1
                            else indObj[companies[j].industry]["Non Platform"] += 1;
                            (assets["Ecommerce"]["Non Platform"]).map(a => {
                                if (data[datasets[i]]["Ecommerce"]["Non Platform"][a] === undefined) {
                                    data[datasets[i]]["Ecommerce"]["Non Platform"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Ecommerce"]["Non Platform"][a]["count"] += 1;
                            });
                        }
                        if (assets["Ecommerce"]["Hosted Solution"] !== undefined) {
                            hostedSolution += 1;
                            if (indObj[companies[j].industry]["Hosted Solution"] === undefined) indObj[companies[j].industry]["Hosted Solution"] = 1
                            else indObj[companies[j].industry]["Hosted Solution"] += 1;
                            (assets["Ecommerce"]["Hosted Solution"]).map(a => {
                                if (data[datasets[i]]["Ecommerce"]["Hosted Solution"][a] === undefined) {
                                    data[datasets[i]]["Ecommerce"]["Hosted Solution"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Ecommerce"]["Hosted Solution"][a]["count"] += 1;
                            });
                        }
                        if (assets["Ecommerce"]["Open Source"] !== undefined) {
                            openSource += 1;
                            if (indObj[companies[j].industry]["Open Source"] === undefined) indObj[companies[j].industry]["Open Source"] = 1
                            else indObj[companies[j].industry]["Open Source"] += 1;
                            (assets["Ecommerce"]["Open Source"]).map(a => {
                                if (data[datasets[i]]["Ecommerce"]["Open Source"][a] === undefined) {
                                    data[datasets[i]]["Ecommerce"]["Open Source"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Ecommerce"]["Open Source"][a]["count"] += 1;
                            });
                        }
                        if (assets["Ecommerce"]["Multi-Channel"] !== undefined) {
                            multiChannel += 1;
                            if (indObj[companies[j].industry]["Multi-Channel"] === undefined) indObj[companies[j].industry]["Multi-Channel"] = 1
                            else indObj[companies[j].industry]["Multi-Channel"] += 1;
                            (assets["Ecommerce"]["Multi-Channel"]).map(a => {
                                if (data[datasets[i]]["Ecommerce"]["Multi-Channel"][a] === undefined) {
                                    data[datasets[i]]["Ecommerce"]["Multi-Channel"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Ecommerce"]["Multi-Channel"][a]["count"] += 1;
                            });
                        }
                        if (assets["Ecommerce"]["Enterprise"] !== undefined) {
                            enterprise += 1;
                            if (indObj[companies[j].industry]["Enterprise"] === undefined) indObj[companies[j].industry]["Enterprise"] = 1
                            else indObj[companies[j].industry]["Enterprise"] += 1;
                            (assets["Ecommerce"]["Enterprise"]).map(a => {
                                if (data[datasets[i]]["Ecommerce"]["Enterprise"][a] === undefined) {
                                    data[datasets[i]]["Ecommerce"]["Enterprise"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Ecommerce"]["Enterprise"][a]["count"] += 1;
                            });
                        }
                        if (assets["Ecommerce"]["SMB Solution"] !== undefined) {
                            smbSolution += 1;
                            if (indObj[companies[j].industry]["SMB Solution"] === undefined) indObj[companies[j].industry]["SMB Solution"] = 1
                            else indObj[companies[j].industry]["SMB Solution"] += 1;
                            (assets["Ecommerce"]["SMB Solution"]).map(a => {
                                if (data[datasets[i]]["Ecommerce"]["SMB Solution"][a] === undefined) {
                                    data[datasets[i]]["Ecommerce"]["SMB Solution"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Ecommerce"]["SMB Solution"][a]["count"] += 1;
                            });
                        }
                        totalEcommerceCount += 1;
                    }

                    if (assets["Payment"] !== undefined) {
                        if (assets["Payment"]["Payments Processor"] !== undefined) {
                            payProcessor += 1;
                            if (indObj[companies[j].industry]["Payments Processor"] === undefined) indObj[companies[j].industry]["Payments Processor"] = 1
                            else indObj[companies[j].industry]["Payments Processor"] += 1;
                            (assets["Payment"]["Payments Processor"]).map(a => {
                                if (data[datasets[i]]["Payment"]["Payments Processor"][a] === undefined) {
                                    data[datasets[i]]["Payment"]["Payments Processor"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Payment"]["Payments Processor"][a]["count"] += 1;
                            });
                        }
                        if (assets["Payment"]["Payment Acceptance"] !== undefined) {
                            payAcceptance += 1;
                            if (indObj[companies[j].industry]["Payment Acceptance"] === undefined) indObj[companies[j].industry]["Payment Acceptance"] = 1
                            else indObj[companies[j].industry]["Payment Acceptance"] += 1;
                            (assets["Payment"]["Payment Acceptance"]).map(a => {
                                if (data[datasets[i]]["Payment"]["Payment Acceptance"][a] === undefined) {
                                    data[datasets[i]]["Payment"]["Payment Acceptance"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Payment"]["Payment Acceptance"][a]["count"] += 1;
                            });
                        }
                        if (assets["Payment"]["Payment Currency"] !== undefined) {
                            payCurrency += 1;
                            if (indObj[companies[j].industry]["Payment Currency"] === undefined) indObj[companies[j].industry]["Payment Currency"] = 1
                            else indObj[companies[j].industry]["Payment Currency"] += 1;
                            (assets["Payment"]["Payment Currency"]).map(a => {
                                if (data[datasets[i]]["Payment"]["Payment Currency"][a] === undefined) {
                                    data[datasets[i]]["Payment"]["Payment Currency"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Payment"]["Payment Currency"][a]["count"] += 1;
                            });
                        }
                        if (assets["Payment"]["Checkout Buttons"] !== undefined) {
                            checkOurButton += 1;
                            if (indObj[companies[j].industry]["Checkout Buttons"] === undefined) indObj[companies[j].industry]["Checkout Buttons"] = 1
                            else indObj[companies[j].industry]["Checkout Buttons"] += 1;
                            (assets["Payment"]["Checkout Buttons"]).map(a => {
                                if (data[datasets[i]]["Payment"]["Checkout Buttons"][a] === undefined) {
                                    data[datasets[i]]["Payment"]["Checkout Buttons"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Payment"]["Checkout Buttons"][a]["count"] += 1;
                            });
                        }
                        if (assets["Payment"]["Payment Acceptance"] !== undefined &&
                            assets["Payment"]["Payment Currency"] !== undefined &&
                            assets["Payment"]["Checkout Buttons"] !== undefined
                        ) {
                            PAPCCO += 1;
                            if (indObj[companies[j].industry]["PAPCCO"] === undefined) indObj[companies[j].industry]["PAPCCO"] = 1
                            else indObj[companies[j].industry]["PAPCCO"] += 1;
                            // (assets["Payment"]["Payments Acceptance"]).map(a => {
                            //     if (data[datasets[i]]["Payment"]["PAPCCO"][a] === undefined) {
                            //         data[datasets[i]]["Payment"]["PAPCCO"][a] = { "name": a, "count": 0 };
                            //     }
                            //     data[datasets[i]]["Payment"]["PAPCCO"][a]["count"] += 1;
                            // });
                        }

                        if (assets["Payment"]["Payments Processor"] !== undefined &&
                            assets["Payment"]["Payment Acceptance"] !== undefined &&
                            assets["Payment"]["Payment Currency"] !== undefined &&
                            assets["Payment"]["Checkout Buttons"] !== undefined
                        ) {
                            PPPAPCCO += 1;
                            if (indObj[companies[j].industry]["PPPAPCCO"] === undefined) indObj[companies[j].industry]["PPPAPCCO"] = 1
                            else indObj[companies[j].industry]["PPPAPCCO"] += 1;
                            // (assets["Payment"]["Payments Currency"]).map(a => {
                            //     if (data[datasets[i]]["Payment"]["PPPAPCCO"][a] === undefined) {
                            //         data[datasets[i]]["Payment"]["PPPAPCCO"][a] = { "name": a, "count": 0 };
                            //     }
                            //     data[datasets[i]]["Payment"]["PPPAPCCO"][a]["count"] += 1;
                            // });
                        }
                        totalPaymentCount += 1;
                    }

                    if (assets["Widgets"] !== undefined) {
                        if (assets["Widgets"]["Live Chat"] !== undefined) {
                            liveChat += 1;
                            if (indObj[companies[j].industry]["Live Chat"] === undefined) indObj[companies[j].industry]["Live Chat"] = 1
                            else indObj[companies[j].industry]["Live Chat"] += 1;
                            (assets["Widgets"]["Live Chat"]).map(a => {
                                if (data[datasets[i]]["Widgets"]["Live Chat"][a] === undefined) {
                                    data[datasets[i]]["Widgets"]["Live Chat"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Widgets"]["Live Chat"][a]["count"] += 1;
                            });
                        }
                        if (assets["Widgets"]["Login"] !== undefined) {
                            customerLogin += 1;
                            if (indObj[companies[j].industry]["Login"] === undefined) indObj[companies[j].industry]["Login"] = 1
                            else indObj[companies[j].industry]["Login"] += 1;
                            // (assets["Widgets"]["Login"]).map(a => {
                            //     if (data[datasets[i]]["Widgets"]["Login"][a] === undefined) {
                            //         data[datasets[i]]["Widgets"]["Login"][a] = { "name": a, "count": 0 };
                            //     }
                            //     data[datasets[i]]["Widgets"]["Login"][a]["count"] += 1;
                            // });
                        }
                        if (assets["Widgets"]["Social Sharing"] !== undefined) {
                            socialSharing += 1;
                            if (indObj[companies[j].industry]["Social Sharing"] === undefined) indObj[companies[j].industry]["Social Sharing"] = 1
                            else indObj[companies[j].industry]["Social Sharing"] += 1;
                            (assets["Widgets"]["Social Sharing"]).map(a => {
                                if (data[datasets[i]]["Widgets"]["Social Sharing"][a] === undefined) {
                                    data[datasets[i]]["Widgets"]["Social Sharing"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Widgets"]["Social Sharing"][a]["count"] += 1;
                            });
                        }
                        if (assets["Widgets"]["Schedule Management"] !== undefined) {
                            scheduleManagement += 1;
                            if (indObj[companies[j].industry]["Schedule Management"] === undefined) indObj[companies[j].industry]["Schedule Management"] = 1
                            else indObj[companies[j].industry]["Schedule Management"] += 1;
                            (assets["Widgets"]["Schedule Management"]).map(a => {
                                if (data[datasets[i]]["Widgets"]["Schedule Management"][a] === undefined) {
                                    data[datasets[i]]["Widgets"]["Schedule Management"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Widgets"]["Schedule Management"][a]["count"] += 1;
                            });
                        }
                        if (assets["Widgets"]["Ticketing System"] !== undefined) {
                            ticketingSystem += 1;
                            if (indObj[companies[j].industry]["Ticketing System"] === undefined) indObj[companies[j].industry]["Ticketing System"] = 1
                            else indObj[companies[j].industry]["Ticketing System"] += 1;
                            (assets["Widgets"]["Ticketing System"]).map(a => {
                                if (data[datasets[i]]["Widgets"]["Ticketing System"][a] === undefined) {
                                    data[datasets[i]]["Widgets"]["Ticketing System"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Widgets"]["Ticketing System"][a]["count"] += 1;
                            });
                        }
                        if (assets["Widgets"]["Bookings"] !== undefined) {
                            bookings += 1;
                            if (indObj[companies[j].industry]["Bookings"] === undefined) indObj[companies[j].industry]["Bookings"] = 1
                            else indObj[companies[j].industry]["Bookings"] += 1;
                            (assets["Widgets"]["Bookings"]).map(a => {
                                if (data[datasets[i]]["Widgets"]["Bookings"][a] === undefined) {
                                    data[datasets[i]]["Widgets"]["Bookings"][a] = { "name": a, "count": 0 };
                                }
                                data[datasets[i]]["Widgets"]["Bookings"][a]["count"] += 1;
                            });
                        }
                        totalWidgetsCount += 1;
                    }

                    if (indObj[companies[j].industry]) {
                        industryObj[datasets[i]]["Advertising"][companies[j].industry] = [
                            companies[j].industry,
                            indObj[companies[j].industry]["Ad Network"] ? (indObj[companies[j].industry]["Ad Network"] / adNetwork) * 100 : 0,
                            indObj[companies[j].industry]["ads txt"] ? (indObj[companies[j].industry]["ads txt"] / adsTxt) * 100 : 0,
                            indObj[companies[j].industry]["Contextual Advertising"] ? (indObj[companies[j].industry]["Contextual Advertising"] / contextualAds) * 100 : 0,
                            indObj[companies[j].industry]["Ad Exchange"] ? (indObj[companies[j].industry]["Ad Exchange"] / adExchange) * 100 : 0,
                            indObj[companies[j].industry]["Facebook Exchange"] ? (indObj[companies[j].industry]["Facebook Exchange"] / fbExchange) * 100 : 0,
                            indObj[companies[j].industry]["Retargeting / Remarketing"] ? (indObj[companies[j].industry]["Retargeting / Remarketing"] / retargetingAds) * 100 : 0
                        ];
                        industryObj[datasets[i]]["Analytics"][companies[j].industry] = [
                            companies[j].industry,
                            indObj[companies[j].industry]["Conversion Optimization"] ? (indObj[companies[j].industry]["Conversion Optimization"] / conversionOpt) * 100 : 0,
                            indObj[companies[j].industry]["Tag Management"] ? (indObj[companies[j].industry]["Tag Management"] / tagMng) * 100 : 0,
                            indObj[companies[j].industry]["Advertiser Tracking"] ? (indObj[companies[j].industry]["Advertiser Tracking"] / adsTracking) * 100 : 0,
                            indObj[companies[j].industry]["Audience Measurement"] ? (indObj[companies[j].industry]["Audience Measurement"] / audienceMeasurement) * 100 : 0,
                            indObj[companies[j].industry]["Lead Generation"] ? (indObj[companies[j].industry]["Lead Generation"] / leadGeneration) * 100 : 0,
                            indObj[companies[j].industry]["Marketing Automation"] ? (indObj[companies[j].industry]["Marketing Automation"] / marketAutomation) * 100 : 0
                        ];
                        industryObj[datasets[i]]["Ecommerce"][companies[j].industry] = [
                            companies[j].industry,
                            indObj[companies[j].industry]["Non Platform"] ? (indObj[companies[j].industry]["Non Platform"] / nonPlatform) * 100 : 0,
                            indObj[companies[j].industry]["Hosted Solution"] ? (indObj[companies[j].industry]["Hosted Solution"] / hostedSolution) * 100 : 0,
                            indObj[companies[j].industry]["Open Source"] ? (indObj[companies[j].industry]["Open Source"] / openSource) * 100 : 0,
                            indObj[companies[j].industry]["Multi-Channel"] ? (indObj[companies[j].industry]["Multi-Channel"] / multiChannel) * 100 : 0,
                            indObj[companies[j].industry]["Enterprise"] ? (indObj[companies[j].industry]["Enterprise"] / enterprise) * 100 : 0,
                            indObj[companies[j].industry]["SMB Solution"] ? (indObj[companies[j].industry]["SMB Solution"] / smbSolution) * 100 : 0
                        ];
                        industryObj[datasets[i]]["Payment"][companies[j].industry] = [
                            companies[j].industry,
                            indObj[companies[j].industry]["Payments Processor"] ? (indObj[companies[j].industry]["Payments Processor"] / payProcessor) * 100 : 0,
                            indObj[companies[j].industry]["Payment Acceptance"] ? (indObj[companies[j].industry]["Payment Acceptance"] / payAcceptance) * 100 : 0,
                            indObj[companies[j].industry]["Payment Currency"] ? (indObj[companies[j].industry]["Payment Currency"] / payCurrency) * 100 : 0,
                            indObj[companies[j].industry]["Checkout Buttons"] ? (indObj[companies[j].industry]["Checkout Buttons"] / checkOurButton) * 100 : 0,
                            indObj[companies[j].industry]["PAPCCO"] ? (indObj[companies[j].industry]["PAPCCO"] / PAPCCO) * 100 : 0,
                            indObj[companies[j].industry]["PPPAPCCO"] ? (indObj[companies[j].industry]["PPPAPCCO"] / PPPAPCCO) * 100 : 0
                        ];
                        industryObj[datasets[i]]["Widgets"][companies[j].industry] = [
                            companies[j].industry,
                            indObj[companies[j].industry]["Live Chat"] ? (indObj[companies[j].industry]["Live Chat"] / liveChat) * 100 : 0,
                            indObj[companies[j].industry]["Login"] ? (indObj[companies[j].industry]["Login"] / customerLogin) * 100 : 0,
                            indObj[companies[j].industry]["Social Sharing"] ? (indObj[companies[j].industry]["Social Sharing"] / socialSharing) * 100 : 0,
                            indObj[companies[j].industry]["Schedule Management"] ? (indObj[companies[j].industry]["Schedule Management"] / scheduleManagement) * 100 : 0,
                            indObj[companies[j].industry]["Ticketing System"] ? (indObj[companies[j].industry]["Ticketing System"] / ticketingSystem) * 100 : 0,
                            indObj[companies[j].industry]["Bookings"] ? (indObj[companies[j].industry]["Bookings"] / bookings) * 100 : 0
                        ];
                    }
                }
            }

            jsonObj[datasets[i]]["Advertising"] = {
                "Type": "Advertising",
                "Count": totalAdvertisingCount,
                "Ad Network": {
                    percent: isNaN(Math.round((adNetwork / totalAdvertisingCount) * 100)) ? 0 : Math.round((adNetwork / totalAdvertisingCount) * 100),
                    count: adNetwork
                },
                "Ads Txt": {
                    percent: isNaN(Math.round((adsTxt / totalAdvertisingCount) * 100)) ? 0 : Math.round((adsTxt / totalAdvertisingCount) * 100),
                    count: adsTxt
                },
                "Contextual Ad": {
                    percent: isNaN(Math.round((contextualAds / totalAdvertisingCount) * 100)) ? 0 : Math.round((contextualAds / totalAdvertisingCount) * 100),
                    count: contextualAds
                },
                "Ad Exchange": {
                    percent: isNaN(Math.round((adExchange / totalAdvertisingCount) * 100)) ? 0 : Math.round((adExchange / totalAdvertisingCount) * 100),
                    count: adExchange
                },
                "Facebook Exchange": {
                    percent: isNaN(Math.round((fbExchange / totalAdvertisingCount) * 100)) ? 0 : Math.round((fbExchange / totalAdvertisingCount) * 100),
                    count: fbExchange
                },
                "Retargeting": {
                    percent: isNaN(Math.round((retargetingAds / totalAdvertisingCount) * 100)) ? 0 : Math.round((retargetingAds / totalAdvertisingCount) * 100),
                    count: retargetingAds
                }
            }

            jsonObj[datasets[i]]["Analytics"] = {
                "Type": "Analytics",
                "Count": totalAnalyticsCount,
                "Conversion Optimization": {
                    percent: isNaN(Math.round((conversionOpt / totalAnalyticsCount) * 100)) ? 0 : Math.round((conversionOpt / totalAnalyticsCount) * 100),
                    count: conversionOpt
                },
                "Tag Management": {
                    percent: isNaN(Math.round((tagMng / totalAnalyticsCount) * 100)) ? 0 : Math.round((tagMng / totalAnalyticsCount) * 100),
                    count: tagMng
                },
                "Advertising Tracking": {
                    percent: isNaN(Math.round((adsTracking / totalAnalyticsCount) * 100)) ? 0 : Math.round((adsTracking / totalAnalyticsCount) * 100),
                    count: adsTracking
                },
                "Audience Measurement": {
                    percent: isNaN(Math.round((audienceMeasurement / totalAnalyticsCount) * 100)) ? 0 : Math.round((audienceMeasurement / totalAnalyticsCount) * 100),
                    count: audienceMeasurement
                },
                "Lead Generation": {
                    percent: isNaN(Math.round((leadGeneration / totalAnalyticsCount) * 100)) ? 0 : Math.round((leadGeneration / totalAnalyticsCount) * 100),
                    count: leadGeneration
                },
                "Marketing Automation": {
                    percent: isNaN(Math.round((marketAutomation / totalAnalyticsCount) * 100)) ? 0 : Math.round((marketAutomation / totalAnalyticsCount) * 100),
                    count: marketAutomation
                }
            }

            jsonObj[datasets[i]]["Ecommerce"] = {
                "Type": "Ecommerce",
                "Count": totalEcommerceCount,
                "Non Platform": {
                    percent: isNaN(Math.round((nonPlatform / totalEcommerceCount) * 100)) ? 0 : Math.round((nonPlatform / totalEcommerceCount) * 100),
                    count: nonPlatform
                },
                "Hosted Solution": {
                    percent: isNaN(Math.round((hostedSolution / totalEcommerceCount) * 100)) ? 0 : Math.round((hostedSolution / totalEcommerceCount) * 100),
                    count: hostedSolution
                },
                "Open Source": {
                    percent: isNaN(Math.round((openSource / totalEcommerceCount) * 100)) ? 0 : Math.round((openSource / totalEcommerceCount) * 100),
                    count: openSource
                },
                "Multi-Channel": {
                    percent: isNaN(Math.round((multiChannel / totalEcommerceCount) * 100)) ? 0 : Math.round((multiChannel / totalEcommerceCount) * 100),
                    count: multiChannel
                },
                "Enterprise": {
                    percent: isNaN(Math.round((enterprise / totalEcommerceCount) * 100)) ? 0 : Math.round((enterprise / totalEcommerceCount) * 100),
                    count: enterprise
                },
                "SMB Solution": {
                    percent: isNaN(Math.round((smbSolution / totalEcommerceCount) * 100)) ? 0 : Math.round((smbSolution / totalEcommerceCount) * 100),
                    count: smbSolution
                }
            }

            jsonObj[datasets[i]]["Payment"] = {
                "Type": "Payment",
                "Count": totalPaymentCount,
                "Payments Processor": {
                    percent: isNaN(Math.round((payProcessor / totalPaymentCount) * 100)) ? 0 : Math.round((payProcessor / totalPaymentCount) * 100),
                    count: payProcessor
                },
                "Payment Acceptance": {
                    percent: isNaN(Math.round((payAcceptance / totalPaymentCount) * 100)) ? 0 : Math.round((payAcceptance / totalPaymentCount) * 100),
                    count: payAcceptance
                },
                "Payment Currency": {
                    percent: isNaN(Math.round((payCurrency / totalPaymentCount) * 100)) ? 0 : Math.round((payCurrency / totalPaymentCount) * 100),
                    count: payCurrency
                },
                "Checkout Buttons": {
                    percent: isNaN(Math.round((checkOurButton / totalPaymentCount) * 100)) ? 0 : Math.round((checkOurButton / totalPaymentCount) * 100),
                    count: checkOurButton
                },
                "PP, PA, PC, CO": {
                    percent: isNaN(Math.round((PPPAPCCO / totalPaymentCount) * 100)) ? 0 : Math.round((PPPAPCCO / totalPaymentCount) * 100),
                    count: PPPAPCCO
                },
                "PA & PC CO": {
                    percent: isNaN(Math.round((PAPCCO / totalPaymentCount) * 100)) ? 0 : Math.round((PAPCCO / totalPaymentCount) * 100),
                    count: PAPCCO
                }
            }

            jsonObj[datasets[i]]["Widgets"] = {
                "Type": "Widgets",
                "Count": totalWidgetsCount,
                "Live Chat": {
                    percent: isNaN(Math.round((liveChat / totalWidgetsCount) * 100)) ? 0 : Math.round((liveChat / totalWidgetsCount) * 100),
                    count: liveChat
                },
                "Customer Login": {
                    percent: isNaN(Math.round((customerLogin / totalWidgetsCount) * 100)) ? 0 : Math.round((customerLogin / totalWidgetsCount) * 100),
                    count: customerLogin
                },
                "Social Sharing": {
                    percent: isNaN(Math.round((socialSharing / totalWidgetsCount) * 100)) ? 0 : Math.round((socialSharing / totalWidgetsCount) * 100),
                    count: socialSharing
                },
                "Schedule Management": {
                    percent: isNaN(Math.round((scheduleManagement / totalWidgetsCount) * 100)) ? 0 : Math.round((scheduleManagement / totalWidgetsCount) * 100),
                    count: scheduleManagement
                },
                "Ticketing System": {
                    percent: isNaN(Math.round((ticketingSystem / totalWidgetsCount) * 100)) ? 0 : Math.round((ticketingSystem / totalWidgetsCount) * 100),
                    count: ticketingSystem
                },
                "Bookings": {
                    percent: isNaN(Math.round((bookings / totalWidgetsCount) * 100)) ? 0 : Math.round((bookings / totalWidgetsCount) * 100),
                    count: bookings
                }
            }

        }

        return res.status(200).json({
            message: "Successful",
            results: jsonObj,
            industry: industryObj,
            ind: indObj,
            industryProvider: data
        });

    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

exports.totalTechnology = async function (req, res) {

    const file_name = req.body.file_name;
    //req.body.file_name;
    const dataset = req.body.dataset;
    const frimographicFilter = req.body.frimographicFilter;
    //req.body.dataset;

    const keyValues = {

        // "Avertising  Network": "Advertising",
        "ads txt": "Advertising",
        // "Ad Exchange": "Advertising",
        "Audience Targeting": "Advertising",
        // "Facebook Exchange": "Advertising",
        // "Ad Server": "Advertising",
        // "Affiliate Programs": "Advertising",
        "Contextual Advertising": "Advertising",
        "Dynamic Creative Optimization": "Advertising",
        "Digital Video Ads": "Advertising",
        "Retargeting / Remarketing": "Advertising",
        // "Header Bidding": "Advertising",

        "Application Performance": "Analytics and Tracking",
        // "A/B Testing": "Analytics and Tracking",
        // "Ad Analytics": "Analytics and Tracking",
        "Conversion Optimization": "Analytics and Tracking",
        "Advertiser Tracking": "Analytics and Tracking",
        "Tag Management": "Analytics and Tracking",
        "Audience Measurement": "Analytics and Tracking",
        "Visitor Count Tracking": "Analytics and Tracking",

        "Non Platform": "Ecommerce",
        "Hosted Solution": "Ecommerce",
        "Open Source": "Ecommerce",
        "Checkout Buttons": "Ecommerce",
        "Payment Acceptance": "Ecommerce",
        "Payments Processor": "Ecommerce",
        // "Payment Currency": "Ecommerce",

        "Live Chat": "Widget",
        "Login": "Widget",
        "Ticketing System": "Widget",
        "Bookings": "Widget",
        "Social Sharing": "Widget",
        "Social Management": "Widget",

        "Cloud Hosting": "Hosting",
        "Cloud PaaS": "Hosting",
        "Dedicated Hostig": "Hosting",
        "Business Email Hosting": "Hosting",
        "Web Hosting Provider Email": "Hosting",
        "Marketing Platform": "Hosting",

        "CRM": "Productivity",
        "Lead Generation": "Productivity",
        "Marketing Automation": "Productivity",
        "Product Recommendations": "Productivity",
        "Feedback Forms and Surveys": "Productivity",
        "Campaign Management": "Productivity"
    };

    let whereFilter = [{ dataset }, { asset: { [Op.ne]: null } }];

    if (file_name !== "Master DB (Golden Source)") whereFilter.push({ file_name });

    frimographicFilter && frimographicFilter.industry && whereFilter.push({ industry: { [Op.or]: frimographicFilter.industry } })
    frimographicFilter && frimographicFilter.main_hq_location && whereFilter.push({ main_hq_location: { [Op.or]: frimographicFilter.main_hq_location } })
    frimographicFilter && frimographicFilter.emp_size && whereFilter.push({ total_personnel: { [Op.or]: frimographicFilter.emp_size } })


    let obj = {}, industryArray = [], total = {};

    var total_store = (function () {
        var storage = {
            children: {},
            count: 0
        };

        function Handler() {

            var self = this;
            this._ck = null;

            this.get = function () {
                if (!self._ck) return 0;
                var source = storage.children;
                var count = 0;
                for (var i = 0; i < self._ck.length; i++) {
                    var key = self._ck[i];
                    if (!source.hasOwnProperty(key)) return 0;
                    count = source[key].count;
                    source = source[key].children;
                }
                return count;
            }

            this.increase = function () {
                var source = storage.children;
                storage.count += 1;
                for (var i = 0; i < self._ck.length; i++) {
                    var key = self._ck[i];
                    source[key] = source[key] ? source[key] : { children: {}, count: 0 };
                    source[key].count += 1;
                    source = source[key].children;
                }
            }

            this.setKeys = function (key_arr) {
                self._ck = key_arr;
                return self;
            }

            this.getStorage = function () {
                return storage;
            }

            this.setStorage = function (store) {
                storage = store;
            }
        }


        var handler = new Handler();
        return function () {
            return handler.setKeys(arguments);
        }

    })();

    try {

        const companies = await Company.findAll({
            where: { [Op.and]: whereFilter },
            attributes: ["id", "asset", "industry"]
        });

        if (companies) {
            for (let i = 0; i < companies.length; i++) {

                industryArray.push(companies[i].industry)
                if (companies[i]["asset"]) {

                    let assets = JSON.parse(companies[i]["asset"]);

                    for (var category in assets) {
                        if (!assets.hasOwnProperty(category)) continue;
                        // if (!keyValues2[category]) continue;

                        var types = assets[category];
                        for (var type in types) {
                            if (!types.hasOwnProperty(type)) continue;
                            // if (keyValues2[category].indexOf(type) == -1) continue;
                            if (!keyValues[type]) continue;
                            category = keyValues[type] ? keyValues[type] : category;


                            var brands = types[type];
                            for (var j = 0; j < brands.length; j++) {
                                var brand = brands[j];
                                // total[category] = total[category] ? total[category] : {};

                                // var total_type = total[category];
                                // total_type[type] = total_type[type] ? total_type[type] : {};

                                // var total_brand = total_type[type];
                                // total_brand[brand] = total_brand[brand] ? total_brand[brand] + 1 : 1;
                                total_store(category, type, brand).increase();
                            }
                        }
                    }

                    // Object.keys(assets).map(value => {

                    //     console.log(assets[value])
                    // })

                    let id = companies[i].id;

                    Object.values(assets).forEach(data => {
                        console.log('data', data);
                        let key = Object.keys(data)[0]; //2nd lvl

                        if (keyValues[key]) { //1st lvl

                            let value = keyValues[key];

                            if (obj[value] === undefined) {

                                obj[value] = {
                                    type: value,
                                    count: 1,
                                    options: [
                                        {
                                            label: key,
                                            count: 1,
                                            id: [id]
                                        }
                                    ],
                                    industries: [
                                        {
                                            label: companies[i]["industry"],
                                            key: key,
                                            count: 1
                                        }
                                    ]
                                };
                            } else {
                                obj[value]["count"] += 1;

                                let isEqual = false;
                                let industryEqual = false;

                                obj[value]["options"].forEach(item => {
                                    if (item["label"] === key) {
                                        item["count"] += 1;
                                        item["id"].push(id);
                                        isEqual = true;
                                    }
                                });

                                obj[value]["industries"].forEach(item => {
                                    if (item["label"] === companies[i]["industry"] && item["key"] === key) {
                                        item["count"] += 1;
                                        industryEqual = true;
                                    }
                                });

                                if (!isEqual) obj[value]["options"].push({ label: key, count: 1, id: [id] });
                                if (!industryEqual) obj[value]["industries"].push({ label: companies[i]["industry"], key: key, count: 1 });
                            }
                        }
                    });
                }
            }

            industryArray = [...new Set(industryArray)]


            // let industryData = []
            // industryArray.forEach(data => industryData.push({ label: data, count: 0 }))

            // Object.values(obj).forEach((item, i) => {
            //     let clone = [...industryData]
            //     if (item.industries) item.industries.forEach(data => {
            //         clone[clone.findIndex(x => x.label === data.label)].count += data.count
            //     })
            //     console.log("clone", clone)
            //     item.industryData = [...clone]
            // }
            // )

            Object.values(obj).forEach((item, i) => {
                item.industryData = industryArray
                Object.keys(total).map(function (key, index) {
                    // let count = 0
                    // Object.keys(total[key]).map(function (nestedKey) {
                    //     if (key === item.type) {
                    //         count += parseInt(Object.values(total[key][nestedKey]))
                    //         item.provider = total[key]
                    //         item.provider.count = count
                    //     }
                    // });
                    if (key === item.type) item.provider = total[key]
                });
                item.companyTotal = total_store().getStorage()
            })


            return res.status(200).json({ message: "Successful", data: Object.values(obj) })
        }
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

// NOT DONE
// exports.digitalPresenceFilter = async function (req, res) {

//     let dataset = req.body.dataset;
//     let file_name = req.body.file_name;

//     let industryName = [];
//     let totalIndustry = 0;
//     let industries;
//     let contact, social, digitalPresence, digitalEngagement

//     try {
//         if (file_name !== "Master DB (Golden Source)") {
//             industries = await db.query(
//                 `SELECT IF(industry IS NULL, 'Not Available', industry) industry, COUNT(1) as count FROM company WHERE dataset IN(:datasets) and file_name LIKE :file_name GROUP BY 1 ORDER BY count DESC`,
//                 {
//                     replacements: { datasets: dataset, file_name: file_name },
//                     type: db.QueryTypes.SELECT
//                 }
//             );
//         } else {
//             industries = await db.query(
//                 `SELECT IF(industry IS NULL, 'Not Available', industry) industry, COUNT(1) as count FROM company WHERE dataset IN(:datasets) GROUP BY 1 ORDER BY count DESC`,
//                 {
//                     replacements: { datasets: dataset },
//                     type: db.QueryTypes.SELECT
//                 }
//             );
//         }

//         for (let i = 0; i < industries.length; i++) {
//             industryName.push(industries[i].industry);
//             totalIndustry += industries[i].count
//         }

//         return res.status(200).json({
//             message: "Successful",
//             results: {
//                 industries: industries,
//                 totalIndustry: totalIndustry,
//                 industryName: industryName
//             }
//         });
//     } catch (error) {
//         return res.status(500).json({ message: error.message });
//     }
// }


