'use strict';

const Sequelize = require('sequelize');
const Op = Sequelize.Op;

//models
const User = require('../models/User');


exports.SignIn = (req, res) => {

    let email = req.body.email;
    let password = req.body.password;

    return User
        .findOne({ email })
        .then(response => {
            if (response) return res.status(200).json({
                message: "Successful",
                user: response
            });
            else return res.status(404).json({
                message: "User not found",
            });
        })
        .catch(error => {
            return res.status(500).json({
                message: "Failed To Login"
            });
        });
};
