'use strict';
const sequelize = require('sequelize');
const Op = sequelize.Op;
const formidable = require('formidable');
const fs = require('fs');
const XLSX = require('xlsx');
const axios = require('axios');

const CompanyItem = require('../models/Company');
const PersonnelItem = require('../models/Personnel');
const db = require('../models/db');





const StoreCompany = function (company) {
    return new Promise(function (resolve, reject) {
        try {
            if (company["company_name"] !== null && company["company_name"] !== undefined && company["source"] !== undefined && company["source"] !== null) {
                let companyObj = {};
                for (let key in company) {
                    if (company[key] !== undefined && company[key] !== null) {
                        companyObj[key] = company[key];
                    }
                }

                // CompanyItem.findOne(
                //     {
                //         where: {
                //             [Op.and]: [
                //                 { company_name: company["company_name"] },
                //                 { website: company["website"] }
                //             ]
                //         }
                //     }
                // ).then(response => {
                //     if (response) {
                //         return response.updateAttributes(companyObj);
                //     }
                // });
                CompanyItem.findOne({
                    where: {
                        [Op.and]: [
                            { company_name: companyObj["company_name"] },
                            { source: companyObj["source"] }
                        ]
                    }
                }).then(response => {
                    if (response) {
                        return response.updateAttributes(companyObj);
                    } else {
                        return CompanyItem.create(companyObj);
                    }
                });
            }
        } catch (error) {
            resolve(true);
        }
    });
}

exports.importCompanies = async function (req, res) {
    try {
        const formData = new formidable.IncomingForm();
        formData.parse(req, async (err, fields, files) => {

            if (err || !files.file) {
                return res.status(500).json({
                    meta: {
                        success: false,
                        message: err.message
                    }
                });
            }


            let wb = XLSX.read(files.file.path, {
                type: "file",
            });

            /* Get first worksheet */
            let wsname = wb.SheetNames[0];
            let ws = wb.Sheets[wsname];
            /* Convert array of arrays */
            let data = XLSX.utils.sheet_to_json(ws, { header: '1', defval: "" });
            res.json({
                meta: {
                    success: true,
                    message: "Data is processing"
                }
            });
            //let datas = [data[0]]
            await Promise.all([...data.map(StoreCompany)]);
        });
    } catch (error) {
        return res.status(500).json({
            meta: {
                success: false,
                message: error.message
            }
        });
    }
}

exports.downloadSampleCompany = async function (req, res) {
    try {
        let path = `./analyze_files/sample_company.xlsx`;
        return res.download(path);
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
}

exports.downloadSamplePersonnel = async function (req, res) {
    try {
        let path = `./analyze_files/sample_company.xlsx`;
        return res.download(path);
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
}

exports.uploadTechno = function (req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        try {
            fs.readFile(files.file.path, {
                encoding: 'utf8',
            }, function (err, content) {
                if (err) {
                    return res.json({
                        meta: {
                            code: 0,
                            success: false,
                            message: err.message,
                        },
                    });
                }

                const wb = XLSX.read(content, {
                    type: 'string',
                    raw: true,
                });

                /* Get first worksheet */
                const wsname = wb.SheetNames[0];
                const ws = wb.Sheets[wsname];
                /* Convert array of arrays */
                const data = XLSX.utils.sheet_to_json(ws, {
                    header: 1,
                });
                if (data.length === 0) {
                    throw 'No Data';
                }

                let obj = {};

                data.forEach(function (row) {
                    if (row.length >= 4) {

                        let website = row[0];
                        let company_name = row[1];

                        if (obj[website] === undefined) {
                            obj[website] = {
                                company_name,
                                website,
                                asset: {},
                                technology_spending: 0
                            };
                        }

                        if (row[2] === "TechnologySpending") {
                            obj[website].technology_spending = row[3];
                        }

                        if (row[2] === "asset") {

                            let asset_name = row[3] ? row[3] : null;
                            let asset_category = row[4] ? row[4] : null;
                            let asset_value = row[5] ? row[5] : null;

                            if (asset_name && obj[website]["asset"][asset_name] === undefined) {
                                obj[website]["asset"][asset_name] = {
                                    [asset_category]: [asset_value]
                                }
                            } else {
                                if (asset_name && obj[website]["asset"][asset_name][asset_category] === undefined) {
                                    obj[website]["asset"][asset_name][asset_category] = [asset_value];
                                } else {
                                    obj[website]["asset"][asset_name][asset_category].push(asset_value);
                                }
                            }
                        }
                    }
                });

                Object
                    .values(obj)
                    .map(async a => {
                        await CompanyItem.update(
                            {
                                asset: JSON.stringify(a.asset),
                                spending: a.technology_spending
                            },
                            {
                                where: {
                                    // company_name: {
                                    //     [Op.like]: a.company_name
                                    // }
                                    [Op.or]: [
                                        {
                                            company_name: {
                                                [Op.like]: a.company_name
                                            }
                                        },
                                        {
                                            website: {
                                                [Op.like]: a.website
                                            }
                                        }
                                    ]
                                }
                            }
                        );
                    });

                return res.json({
                    meta: {
                        code: 200,
                        success: true,
                        message: 'Uploaded successfully',
                    }
                });

            });
        } catch (e) {
            return res.json({
                meta: {
                    code: 0,
                    success: false,
                    message: e.message,
                }
            })
        }
    });
}

exports.uploadGoogleIndonesiaCompanies = function (req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        try {
            fs.readFile(files.file.path, {
                encoding: 'utf8',
            }, function (err, content) {
                if (err) {
                    return res.json({
                        meta: {
                            code: 0,
                            success: false,
                            message: err.message,
                        },
                    });
                }
                const wb = XLSX.read(content, {
                    type: 'string',
                    raw: true,
                });

                /* Get first worksheet */
                const wsname = wb.SheetNames[0];
                const ws = wb.Sheets[wsname];
                /* Convert array of arrays */
                const data = XLSX.utils.sheet_to_json(ws, {
                    header: 1,
                });
                if (data.length === 0) {
                    throw 'No Data';
                }

                let results = [];

                data.forEach(function (row) {
                    let company = {};
                    company.company_name = row[0];
                    company.description = row[1];
                    company.dataset = row[2];
                    company.address = row[3];
                    company.industry = row[4];
                    company.company_email_address = row[5];
                    company.main_line_number = row[6];
                    company.organization_type = row[7];
                    company.year_of_operation = row[8];
                    company.main_hq_location = row[9];
                    company.total_personnel = row[10];
                    company.website = row[11];
                    company.facebook = row[12];
                    company.twitter = row[13];
                    company.linkedIn = row[14];
                    company.product_service = row[15];
                    //    company.revenue_size = row[16];
                    company.file_name = row[17];
                    company.searchable = null;
                    company.overall_knapshot_score = -1;
                    company.searchability_score = -1;
                    company.activity_score = -1;
                    company.consistency_score = -1;
                    company.company_status = null;
                    company.has_funding = null;
                    company.business_type = "cannot verify";
                    company.industry_second_level = "cannot verify";
                    company.industry_third_level = "cannot verify";
                    company.year_in_operation = null;
                    company.total_offices_region = -1;
                    company.total_offices_cop = -1;
                    company.management = -1;
                    company.staff = -1;
                    company.no_of_directory_presence = -1;
                    company.digital_presence_analysis = null;
                    company.fax = null;
                    company.agency_status = null;
                    company.instagram = null;
                    company.data_quality = null;
                    company.partners = null;
                    company.asset = "{}";
                    company.client_industries = null;
                    company.spending = null;
                    results.push(company);
                });

                results.map(data => {
                    //   CompanyItem.create(data);
                });

                return res.json({
                    meta: {
                        code: 200,
                        success: true,
                        message: 'Uploaded successfully',
                    },
                    data: results
                });
            });
        } catch (e) {
            return res.json({
                meta: {
                    code: 0,
                    success: false,
                    message: e.message,
                }
            })
        }
    });
}

exports.uploadCompanies = function (req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        try {
            fs.readFile(files.file.path, {
                encoding: 'utf8',
            }, function (err, content) {
                if (err) {
                    return res.json({
                        meta: {
                            code: 0,
                            success: false,
                            message: err.message,
                        },
                    });
                }
                const wb = XLSX.read(content, {
                    type: 'string',
                    raw: true,
                });

                /* Get first worksheet */
                const wsname = wb.SheetNames[0];
                const ws = wb.Sheets[wsname];
                /* Convert array of arrays */
                const data = XLSX.utils.sheet_to_json(ws, {
                    header: 1,
                });
                if (data.length === 0) {
                    throw 'No Data';
                }

                let companyObj = {};

                for (let i = 0; i < data.length; i++) {

                    let company_name = data[i][0];
                    let key = convertCamelToSnakeCase(data[i][1]);
                    let value = data[i][2];

                    if (companyObj[company_name] === undefined) companyObj[company_name] = {};

                    if (key === "social") {
                        let socialKey = convertCamelToSnakeCase(data[i][2]);
                        let socialValue = data[i][3];
                        if (socialKey === "linkedin") socialKey = "linkedIn";
                        companyObj[company_name][socialKey] = socialValue;
                    } else {
                        companyObj[company_name]["company_name"] = company_name;
                        companyObj[company_name][key] = value;
                    }
                }

                let results = Object.values(companyObj);

                for (let j = 0; j < results.length; j++) {

                    CompanyItem.findOne({
                        where: {
                            [Op.and]: [
                                { company_name: results[j]["company_name"] },
                                { source: results[j]["source"] ? results[j]["source"] : null }
                            ]
                        }
                    }).then(response => {
                        if (response) {
                            return response.updateAttributes(results[j]);
                        } else {
                            return CompanyItem.create(results[j]);
                        }
                    });
                }

                return res.json({
                    meta: {
                        code: 200,
                        success: true,
                        message: 'Uploaded successfully',
                    },
                    results: Object.values(companyObj)
                });
            });
        } catch (e) {
            return res.json({
                meta: {
                    code: 0,
                    success: false,
                    message: e.message,
                }
            })
        }
    });
}

function convertCamelToSnakeCase(text) {
    return text.replace(/(?:^|\.?)([A-Z])/g, function (x, y) {
        return "_" + y.toLowerCase()
    }).replace(/^_/, "");
}

