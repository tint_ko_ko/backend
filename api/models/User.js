'use strict';
const Sequelize = require('sequelize');

//models
const db = require('./db');

const User = db.define('user',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        linkedin_id: Sequelize.STRING,
        firstname: Sequelize.STRING,
        lastname: Sequelize.STRING,
        contactnumber: Sequelize.STRING,
        password: Sequelize.STRING,
        email: Sequelize.STRING,
        accesslevel: Sequelize.STRING,
        username: Sequelize.STRING,
        forgotpassword: Sequelize.STRING,
    },
    {
        timestamps: false,
        freezeTableName: true,
    }
);

module.exports = User;